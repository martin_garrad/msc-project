#!/usr/bin/env python
import Environments.MSD.msd_environment
import numpy as np
import random
import matplotlib as mpl
import matplotlib.pyplot as plt 
import seaborn as sn
import time
import copy
import Environments.MSD.costs
import basis
import Environments.MSD.output
import os
import yaml
import Environments.MSD.tasks

AGENTS = ["StochasticHillclimber", "REINFORCE_BASELINE_MOM", "REINFORCE_BASELINE_RPROP", "ENAC_BASELINE_RPROP", "ENAC", "REINFORCE_SIMPLE", "REINFORCE_BASELINE", "REINFORCE_OPTIMAL", "ENAC_RPROP", "ENAC_BASELINE"]

class Agent:
    '''Superclass for all learning agents
    Contains evaluation and plotting methods'''
    def __init__(self, environment, console=None):

        #Environment defines the task for the agent to learn. Given a policy, a number of episodes are performed
        #within the environment. The agent then updates the policy based on the rewards given by the environment.

        self.environment = environment
        self.num_weights = self.environment.num_weights
        self.weights = self.environment.weights

        #By setting self.console, printed output is sent to that object rather than the console
        self.console = console

        #Used to store history of target weights
        self.target_weights = [[] for _ in range(self.num_weights)]

        #Used to store history of costs
        self.costs = []

        #Used to store best weights found during learning
        self.best_weights = []

    def plot_best(self, fig=None):

        #Displays the environment with the best policy found so far applied

        self.environment.update(self.best_weights)
        self.environment.plot(fig=fig)

    def plot_learning(self, fig=None, show=False):

        #Plots data related to the agent's learning
        #Displays the cost per learning iteration, the absolute change in weights per iteration and the current cost per timestep
        
        if fig == None:
            fig = plt.figure()
            show = True

        fig.clear()

        ax = fig.add_subplot(1, 3, 1)
        ax.plot(self.costs)
        ax.set_ylabel('Cost', fontsize=12)
        ax.set_xlabel('Iteration', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)

        ax = fig.add_subplot(1, 3, 2)
        #Plot each weight history on the same axis
        #Consider adding some normalisation here
        for weight_history in self.target_weights:
            ax.plot([w/weight_history[0] for w in weight_history])
        ax.set_ylabel('Weights', fontsize=12)
        ax.set_xlabel('Iteration', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)

        ax = fig.add_subplot(1, 3, 3)
        self.environment.plot_cost(ax)

        if show:
            fig.show()

        fig.tight_layout()

    def plot_weights(self, fig=None, show=False):

        if fig is None:
            fig = plt.figure()
            show = True

        for (index, h) in enumerate(self.target_weights):

            ax = fig.add_subplot(1, self.num_weights, index)
            ax.plot([w/h[0] for w in h], 'r')

        ax.set_ylabel('Weights', fontsize=12)
        ax.set_xlabel('Iteration', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

        fig.tight_layout()

    def evaluate(self, panels=None):

        #Write metadata
        self.panels = panels

        results_folder = self.make_folder()

        try:
            meta = self.make_metadata()
            meta['reference'] = self.environment.reference.write_metadata()
            meta['initial target'] = self.environment.target.write_metadata()
        except:
            pass

        #Create environment
        self.environment.ep_length = self.ep_length
        initial_cost = self.environment.episode()
        self.write("Initial cost = %f\n" % initial_cost)
        
        if panels is None:
            self.sys_fig = plt.figure()
            self.sys_fig.show()

            self.learn_fig = plt.figure()
            self.learn_fig.show()
        else:
            self.sys_fig = panels[0].fig
            self.learn_fig = panels[1].fig

        #Save initial system plot

        self.environment.plot(self.sys_fig)
        
        self.sys_fig.savefig(results_folder + "initial.png")
        
        #Perform episode and store results
        self.learn()
        
        #Save final system plot
        self.environment.plot(self.sys_fig)
        
        self.sys_fig.savefig(results_folder + "final.png")
        
        #Store learning rate plot
        
        self.plot_learning(self.learn_fig)
        
        self.learn_fig.savefig(results_folder + "learning_rate.png")

        #Get final system metadata
        meta['final target'] = self.environment.target.write_metadata()

        #Write metadata to file
        results_folder = self.make_folder()

        self.write_metadata(meta, results_folder)

        final_cost = self.costs[-1]

        return (initial_cost, final_cost)

    def write(self, string):

        #Either writes output to a console object or prints directly to terminal

        if self.console:
            self.console.write(string)
        else:
            print(string)


    def make_folder(self):

        #Creates a folder for storing metadata

        result_folder = "Results/" + self.name + " " + time.strftime("%d-%m-%Y %H:%M", time.gmtime()) + "/"
        if not os.path.exists(result_folder):
            os.makedirs(result_folder)

        return result_folder

    def write_metadata(self, data, folder):

        #Writes metadata to the given folder

        f = folder + "meta.yaml"

        with open(f, 'w') as outfile:
            outfile.write(yaml.dump(data))

    def make_metadata(self):

        #Creates a dictionary of metadata

        meta = dict()

        meta['Agent name'] = self.name
        meta['Target output'] = self.environment.target.output_type
        meta['Target type'] = self.environment.target.name
        meta['Cost'] = 'mse'
        meta['Episode length'] = self.ep_length

        try:
            meta['learning_rate'] = self.rate
            meta['learning_rate_decay'] = self.learning_rate_decay
        except:
            pass

        return meta

class Hillclimber(Agent):

    #Hillclimbers learn purely based on function calls - no gradients are used.

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)

    def learn(self):

        #Perform an initial episode to get a baseline cost
        best_cost = self.environment.episode()

        step = 0.0
        self.step_size = self.learning_rate

        #Mean Learning loop
        while step < self.max_steps and best_cost > self.convergence_threshold:

            step += 1.0
            #Gradually decrease size of parameter perturbation
            self.step_size *= self.learning_rate_decay
            #Store old weights
            old_weights = copy.deepcopy(self.weights)
            #Update to new weights#
            self.update_weights()
            #Evaluate new weights
            cost = self.environment.episode()
            self.costs.append(cost)
            #Store history of weight changes
            for (index, weight_list) in enumerate(self.target_weights):
                weight_list.append(self.environment.weights[index])
            #Plot learning and current policy
            self.environment.plot(self.sys_fig)
            self.panels[0].canvas.draw()
            self.plot_learning(self.learn_fig)
            self.panels[1].canvas.draw()

            #Accept or reject new weights only if cost has improved
            if cost >= best_cost:
                self.environment.update(old_weights)
            else:
                best_cost = cost
                self.write("On step %d I found cost %.2f with step size %.2f\n" % (step, best_cost, self.step_size))
                self.write("New weights are: %s" % [".2f" % w for w in self.weights])

class StochasticHillclimber(Hillclimber):

    #Stochastic Hillclimber makes random perturbations to parameters

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)
        self.max_steps = 100
        self.min_steps = 1
        self.learning_rate = 10.0
        self.learning_rate_decay = 0.99
        self.ep_length = 10.0
        self.convergence_threshold = 0.01
        self.num_rollouts = 1
        self.name = "StochasticHillclimber"

    def update_weights(self):

        self.environment.perturb([self.step_size for i in range(self.num_weights)])
            

class PolicyGradient(Agent):

    #Policy Gradient methods exploit the likelihood ratio trick to calculate a gradient of the cost function w.r.t policy parameters

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)
        #Steps is used to weight changes to each parameter.
        self.steps = [1.0 for _ in range(self.num_weights)]
        self.max_step = 1.0

    def learn(self):

        step = 0
        grad = [9999 for w in self.environment.weights]
        new_weights = [1.0 for w in self.environment.weights]

        #Main Learning Loop
        while step < self.max_steps or step < self.min_steps:

            step += 1
            #Calculate gradient
            grad = self.cost_gradient()

            #Restrict size of update in weight space.
            changes = [min(max(-self.max_step, self.steps[i] * self.learning_rate * grad[i]), self.max_step) for (i, w) in enumerate(self.environment.weights)]
            print changes
            #Update weights
            new_weights = [w + changes[i] for (i, w) in enumerate(self.environment.weights)]

            print new_weights
            
            self.write("Gradient is: %s\n" % (["%.2f" % g for g in grad]))
            self.environment.update(new_weights)
            #Decrease learning rate
            self.learning_rate *= self.learning_rate_decay
            #Evaluate new weights
            cost = self.environment.episode()


            self.write("New weights are: %s\n" % ["%.2f" % w for w in self.environment.weights])
            new_weights = copy.deepcopy(self.environment.weights)
            self.write("On step %d I found cost %f\n" % (step, cost))

            self.costs.append(cost)
            for (index, weight_list) in enumerate(self.target_weights):
                weight_list.append(new_weights[index])

            
            self.environment.plot(self.sys_fig)
            self.panels[0].canvas.draw()
            self.plot_learning(self.learn_fig)
            self.panels[1].canvas.draw()

            #End if something goes wrong!
            if np.isnan(cost):
                break
            

class PolicyGradientMomentum(Agent):

    #Momentum methods change the velocity of the weight changes rather than directly updating weights

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)

        #self.steps = [1.0 for _ in range(self.num_weights)]
        self.momentum_decay = 0.8
        self.correction = 0.05


    def learn(self):

        step = 0
        momentum = [0.0 for w in self.environment.weights]
        grad = [0.0 for w in self.environment.weights]
        new_weights = [1.0 for w in self.environment.weights]


        #Main learning loop
        while step < self.min_steps:

            step += 1

            #Calculate new weights
            #Begin with a large move in the previous direction
            new_weights = [w + (momentum[i]) for (i, w) in enumerate(self.environment.weights)]
            #Calculate gradient
            print new_weights
            grad = self.cost_gradient()
            #Use gradient to make a correction to the previous move
            new_weights = [w + (self.steps[i] * self.correction * self.learning_rate * grad[i]) for (i, w) in enumerate(new_weights)]
            print new_weights
            #Update momentum terms
            momentum = [(self.momentum_decay*m)+(g*s*self.learning_rate) for (m, g, s) in zip(momentum, grad, self.steps)]
            self.write("Momentum is: %s\n" % (["%.2f" % m for m in momentum]))
            self.write("Gradient is: %s\n" % (["%.2f" % g for g in grad]))
 
            self.environment.update(new_weights)
            self.learning_rate *= self.learning_rate_decay

            cost = self.environment.episode()

            self.write("New weights are: %s\n" % ["%.2f" % w for w in self.environment.weights])
            self.write("On step %d I found cost %f\n" % (step, cost))

            self.costs.append(cost)
            for (index, weight_list) in enumerate(self.target_weights):
                weight_list.append(new_weights[index])

            self.environment.plot(self.sys_fig)
            self.panels[0].canvas.draw()
            self.plot_learning(self.learn_fig)
            self.panels[1].canvas.draw()

class PolicyRmsProp(Agent):

    #RMS Prop follows the sign of the gradient instead of the gradient
    #Step size is adaptive according to changes in the sign of the gradient
    #In general, performance is worse than standard gradient methods, but is useful for
    #quick testing as there is no need to tune learning rates.

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)

    def get_direction(self, g):

        #Returns direction of gradient
        if g > 0:
            return 1.0
        else:
            if g < 0:
                return -1.0
            else:
                return 0.0

    def get_rate(self, rate, grad, old_grad):

        #Checks for sign change in gradient. If gradient changes sign, step size is reduced.
        #Otherwise, step size is increased.
        product = old_grad * grad

        if product > 0:
            return rate * self.learning_step_up
        else:
            if product < 0:
                return rate * self.learning_step_down
            else:
                return rate

    def learn(self):

        step = 0

        learning_rates = [0.1 for w in self.environment.weights]
        self.learning_step_down = 0.9
        self.learning_step_up = 1.1

        grad = [0.0 for w in self.environment.weights]
        new_weights = [1.0 for w in self.environment.weights]

        #Main learning loop
        while np.sum([g/w for (g, w) in zip(grad, new_weights)]) > self.convergence_threshold or step < self.min_steps:

            step += 1
            old_grad = copy.deepcopy(grad)
            grad = self.cost_gradient()
            directions = [self.get_direction(g) for g in grad]
            new_weights = [w + (learning_rates[i] * directions[i]) for (i, w) in enumerate(self.environment.weights)]

            learning_rates = [self.get_rate(r, g, o_g) for (r, g, o_g) in zip(learning_rates, grad, old_grad)]


            self.write("Gradient is: %s\n" % ["%.2f" % g for g in grad])

            self.environment.update(new_weights)

            cost = np.mean([self.environment.episode() for _ in range(10)])

            self.write("New weights are: %s\n" % ["%.2f" % w for w in self.environment.weights])
            self.write("On step %d I found cost %f\n" % (step, cost))

            self.costs.append(cost)
            for (index, weight_list) in enumerate(self.target_weights):
                weight_list.append(new_weights[index])

            
            self.environment.plot(self.sys_fig)
            self.panels[0].canvas.draw()
            self.plot_learning(self.learn_fig)
            self.panels[1].canvas.draw()


class REINFORCE_SIMPLE(PolicyGradient):

    def __init__(self, environment, console=None):
        PolicyGradient.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 1.0
        self.learning_rate = -0.001
        self.learning_rate_decay = 0.99
        self.name = "REINFORCE_SIMPLE"
        self.num_rollouts = 5
        self.steps = [1.0, 1.0, 1.0]

    def cost_gradient(self):

        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode(self.ep_length)

        dw = [0.0 for _ in range(self.num_weights)]

        for r in self.environment.rollouts:
            for t in range(len(r.states)):
                score = r.actions[t] - r.means[t]
                dw = [w + (r.total_cost * ((score*feature)/(r.variances[t]*self.num_rollouts))) for (w, feature) in zip(dw, r.activations[t])]

        return dw

class REINFORCE_BASELINE(PolicyGradient):

    def __init__(self, environment, console=None):
        PolicyGradient.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.0000001
        self.learning_rate = -0.05
        self.learning_rate_decay = 0.99
        self.name = "REINFORCE_BASELINE"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 0.10, 0.1, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):

        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        num = [0.0 for _ in range(self.num_weights)]
        denom = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            g_squared = [0.0 for _ in range(self.num_weights)]
            for t in range(len(r.states)):
                g_squared = [g + ((r.actions[t] - r.means[t])*a) for (g, a) in zip(g_squared, r.activations[t])]
            num = [n + (r.total_cost * g**2)/self.num_rollouts for (n, g) in zip(num, g_squared)]
            denom= [d + (g**2)/self.num_rollouts for (d, g) in zip(denom, g_squared)]

        baseline = [(n/d) for (n, d) in zip(num, denom)]

        dw = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            for t in range(len(r.states)):
                score = r.actions[t] - r.means[t]
                '''
                print ''
                print t
                print r.actions[t]
                print r.means[t]
                print r.total_cost
                print score
                print r.activations[t]
                '''
                dw = [w + ((r.total_cost-b) * ((score*feature)/(r.variances[t]*self.num_rollouts))) for (w, feature, b) in zip(dw, r.activations[t], baseline)]

        return dw

class ENAC(PolicyGradient):

    def __init__(self, environment, console=None):
        PolicyGradient.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.1
        self.learning_rate = 5.0
        self.learning_rate_decay = 1.0
        self.name = "ENAC"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 0.10, 0.1, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):
        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        X = []
        Yt = []
        for r in self.environment.rollouts:
            dw = [0.0 for _ in range(self.num_weights)]
            for t in range(len(r.states)):
                dw = [w + (r.actions[t]-r.means[t])*(a/(r.variances[t]**2)) for a, w in zip(r.activations[t], dw)]
            dw.append(1)
            X.append(dw)
            Yt.append(r.total_cost)

        X = np.array(X)
        Y = np.array(Yt).T


        grad = np.dot(np.linalg.inv(np.dot(X.T, X)), np.dot(X.T, Y))

        return grad.T[0:-1]

class ENAC_BASELINE(PolicyGradient):

    def __init__(self, environment, console=None):
        PolicyGradient.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.1
        self.learning_rate = -1.0
        self.learning_rate_decay = 0.99
        self.name = "ENAC_BASELINE"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 0.10, 0.1, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):
        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        fisher_M = np.zeros((self.num_weights, self.num_weights))
        vanilla_G = np.zeros((1, self.num_weights))
        eligibility = np.zeros((1, self.num_weights))
        average_cost = 0.0

        for r in self.environment.rollouts:

            psi = np.zeros((1, self.num_weights))

            for t in range(len(r.states)):
                dpsi = np.array([r.actions[t]-r.means[t]*(a/r.variances[t]**2) for a in r.activations[t]])
                psi = np.add(psi, dpsi)

            dfisher = np.dot(psi.T, psi)
            fisher_M = np.add(fisher_M, dfisher) 

            vanilla_G  = np.add(vanilla_G, np.array([p*r.total_cost for p in psi]))

            eligibility = np.add(eligibility, psi)

            average_cost += r.total_cost

        fisher_M *= 1./self.num_rollouts
        vanilla_G *= 1./self.num_rollouts
        average_cost *= 1./self.num_rollouts
        eligibility *= 1./self.num_rollouts



        Q =  (1 + np.dot(np.dot(eligibility, np.linalg.inv(self.num_rollouts * fisher_M - np.dot(eligibility.T, eligibility))), eligibility.T)) * 1./self.num_rollouts
        b = Q * (average_cost - np.dot(np.dot(eligibility, np.linalg.inv(fisher_M)), vanilla_G.T))

        gradient = np.add(vanilla_G, -1*eligibility*b)

        enac_grad =  np.dot(np.linalg.inv(fisher_M), gradient.T).T

        print enac_grad[0]

        return enac_grad[0]

class REINFORCE_BASELINE_MOM(PolicyGradientMomentum):

    def __init__(self, environment, console=None):
        PolicyGradientMomentum.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.0000001
        self.learning_rate = -0.005
        self.learning_rate_decay = 0.99
        self.name = "REINFORCE_BASELINE_RMS"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 1.0, 0.1, 1.0, -1.0, -1.0, -1.0]


    def cost_gradient(self):

        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        num = [0.0 for _ in range(self.num_weights)]
        denom = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            g_squared = [0.0 for _ in range(self.num_weights)]
            for t in range(len(r.states)):
                g_squared = [g + ((r.actions[t] - r.means[t])*a) for (g, a) in zip(g_squared, r.activations[t])]
            num = [n + (r.total_cost * g**2)/self.num_rollouts for (n, g) in zip(num, g_squared)]
            denom= [d + (g**2)/self.num_rollouts for (d, g) in zip(denom, g_squared)]

        baseline = [(n/d) for (n, d) in zip(num, denom)]

        dw = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            for t in range(len(r.states)):
                score = r.actions[t] - r.means[t]
                '''
                print ''
                print t
                print r.actions[t]
                print r.means[t]
                print r.total_cost
                print score
                print r.activations[t]
                '''
                dw = [w + ((r.total_cost-b) * ((score*feature)/(r.variances[t]*self.num_rollouts))) for (w, feature, b) in zip(dw, r.activations[t], baseline)]

        return dw

class REINFORCE_BASELINE_RPROP(PolicyRmsProp):

    def __init__(self, environment, console=None):
        PolicyRmsProp.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.0000001
        self.learning_rate = -0.05
        self.learning_rate_decay = 0.99
        self.name = "REINFORCE_BASELINE_RMS"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):

        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        num = [0.0 for _ in range(self.num_weights)]
        denom = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            g_squared = [0.0 for _ in range(self.num_weights)]
            for t in range(len(r.states)):
                g_squared = [g + ((r.actions[t] - r.means[t])*a) for (g, a) in zip(g_squared, r.activations[t])]
            num = [n + (r.total_cost * g**2)/self.num_rollouts for (n, g) in zip(num, g_squared)]
            denom= [d + (g**2)/self.num_rollouts for (d, g) in zip(denom, g_squared)]

        baseline = [(n/d) for (n, d) in zip(num, denom)]

        dw = [0.0 for _ in range(self.num_weights)]
        for r in self.environment.rollouts:
            for t in range(len(r.states)):
                score = r.actions[t] - r.means[t]
                '''
                print ''
                print t
                print r.actions[t]
                print r.means[t]
                print r.total_cost
                print score
                print r.activations[t]
                '''
                dw = [w + ((r.total_cost-b) * ((score*feature)/(r.variances[t]*self.num_rollouts))) for (w, feature, b) in zip(dw, r.activations[t], baseline)]

        return dw

class ENAC_RPROP(PolicyRmsProp):

    def __init__(self, environment, console=None):
        PolicyRmsProp.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 1000
        self.ep_length = 10.0
        self.convergence_threshold = 0.1
        self.learning_rate = 0.005
        self.learning_rate_decay = 0.99
        self.name = "ENAC_RPROP"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):
        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        X = []
        Yt = []
        for r in self.environment.rollouts:
            dw = [0.0 for _ in range(self.num_weights)]
            for t in range(len(r.states)):
                dw = [w + (r.actions[t]-r.means[t])*(a/(r.variances[t]**2)) for a, w in zip(r.activations[t], dw)]
            dw.append(1)
            X.append(dw)
            Yt.append(r.total_cost)

        X = np.array(X)
        Y = np.array(Yt).T

        grad = np.dot(np.linalg.inv(np.dot(X.T, X)), np.dot(X.T, Y.T))

        return grad.T[0:-1]

class ENAC_BASELINE_RPROP(PolicyRmsProp):

    def __init__(self, environment, console=None):
        PolicyRmsProp.__init__(self, environment, console=console)
        self.min_steps = 100
        self.max_steps = 100
        self.ep_length = 10.0
        self.convergence_threshold = 0.1
        self.learning_rate = -1.0
        self.learning_rate_decay = 0.99
        self.name = "ENAC_BASELINE_RPROP"
        self.num_rollouts = 10
        self.steps = [1.0, 1.0, 0.10, 0.1, -1.0, -1.0, -1.0, -1.]

    def cost_gradient(self):
        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        fisher_M = np.zeros((self.num_weights, self.num_weights))
        vanilla_G = np.zeros((1, self.num_weights))
        eligibility = np.zeros((1, self.num_weights))
        average_cost = 0.0

        for r in self.environment.rollouts:

            psi = np.zeros((1, self.num_weights))

            for t in range(len(r.states)):
                dpsi = np.array([r.actions[t]-r.means[t]*(a/r.variances[t]**2) for a in r.activations[t]])
                psi = np.add(psi, dpsi)

            dfisher = np.dot(psi.T, psi)
            fisher_M = np.add(fisher_M, dfisher) 

            vanilla_G  = np.add(vanilla_G, np.array([p*r.total_cost for p in psi]))

            eligibility = np.add(eligibility, psi)

            average_cost += r.total_cost

        fisher_M *= 1./self.num_rollouts
        vanilla_G *= 1./self.num_rollouts
        average_cost *= 1./self.num_rollouts
        eligibility *= 1./self.num_rollouts



        Q =  (1 + np.dot(np.dot(eligibility, np.linalg.inv(self.num_rollouts * fisher_M - np.dot(eligibility.T, eligibility))), eligibility.T)) * 1./self.num_rollouts
        b = Q * (average_cost - np.dot(np.dot(eligibility, np.linalg.inv(fisher_M)), vanilla_G.T))

        gradient = np.add(vanilla_G, -1*eligibility*b)

        enac_grad =  np.dot(np.linalg.inv(fisher_M), gradient.T).T

        return enac_grad[0]

'''
Super class for expectation-maximisation approaches to agents
'''
class EM(Agent):

    def __init__(self, environment, console=None):
        Agent.__init__(self, environment, console=console)
        #Steps is used to weight changes to each parameter.
    def learn(self):

        step = 0
        old_weights = [9999 for w in self.environment.weights]
        new_weights = [1.0 for w in self.environment.weights]

        #Main Learning Loop
        while np.sum([g/w for (g, w) in zip(old_weights, new_weights)]) > self.convergence_threshold or step < self.min_steps:

            step += 1
            #Calculate gradient
            grad = self.cost_gradient()

            #Update weights
            old_weights = copy.deepcopy(self.environment.weights)
            new_weights = [1000. * grad[i] for (i, w) in enumerate(self.environment.weights)]

            self.write("Gradient is: %s\n" % ["%.2f" % g for g in grad])
            self.environment.update(new_weights)
            #Evaluate new weights
            cost = self.environment.episode()


            self.write("New weights are: %s\n" % ["%.2f" % w for w in self.environment.weights])
            new_weights = copy.deepcopy(self.environment.weights)
            self.write("On step %d I found cost %f\n" % (step, cost))

            self.costs.append(cost)
            for (index, weight_list) in enumerate(self.target_weights):
                weight_list.append(new_weights[index])

            
            self.environment.plot(self.sys_fig)
            self.panels[0].canvas.draw()
            self.plot_learning(self.learn_fig)
            self.panels[1].canvas.draw()

            #End if something goes wrong!
            if np.isnan(cost):
                break


class RWR(EM):
    def __init__(self, environment, console):
        EM.__init__(self, environment, console=None)
        self.min_steps = 100
        self.max_steps = 1000
        self.ep_length = 10.0
        self.convergence_threshold = 0.1
        self.name = "RWR"
        self.num_rollouts = 5
        self.console = console

    def cost_gradient(self):

        #Perform rollouts
        self.environment.clear_rollouts()

        for _ in range(self.num_rollouts):
            self.environment.episode()

        num= [0.0 for _ in self.weights]
        D= np.zeros([len(self.weights), len(self.weights)])
        M = float(self.num_rollouts)
        #Loop through episodes
        for r in self.environment.rollouts:
            #Loop through trajectory
            phi_d = np.zeros([len(self.weights), len(self.weights)])
            phi_n = [0.0 for _ in self.weights]

            N = len(r.states)
            for t in range(N):
                phi_n = [(pn + (r.actions[t] * a)/N) for (pn, a) in zip(phi_n, r.activations[t])]
                phi_d += np.matrix(r.activations[t]).T*np.matrix(r.activations[t])
            
            D +=  (1./M)*phi_d
            num =  [n + (1./M)*pn for (n, pn) in zip(num, phi_n)]

        grad = np.dot(np.linalg.inv(D).T, np.matrix(num).T)

        print grad.A1
        #raw_input()

        return grad.A1

class Power(EM):
    def __init__(self, environment, console):
        Power.__init__(self, environment, console=None)





