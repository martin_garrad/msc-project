#!/usr/bin/env python
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sn 
import msd
import basis
import costs
import output
import random
import wx
import wx.lib.scrolledpanel
import types

'''The Rollout class is responsible for storing information relating to a single episode'''
class Rollout:
    def __init__(self, environment):

        self.cost = environment.cost
        self.total_cost = environment.cost.total_cost
        self.costs = environment.cost.cost_list
        #Future costs represent the sum of costs after a timestep
        self.future_costs = environment.cost.future_costs
        self.states = environment.target.history
        #Actions is a list of the actual forces applied, while means are the centres of the gaussians from which the actions are drawn
        self.actions = environment.target.actions
        self.means = environment.target.means
        self.variances = environment.target.variances
        self.activations = environment.target.activations
        self.output = environment.target_output

        self.reference_output = environment.reference_output

'''This class represents obstacles in the environment that should be avoided'''

class Obstacle:
    def __init__(self, timesteps, region, weight, hard=False):
        self.timesteps = timesteps
        self.region = region
        self.weight = weight
        #This flag does nothing as of yet, but will eventually determine how the MSD system handles interaction with the object
        self.hard = hard

'''
This class is a superclass for any reference given to the environment. 
When using an MSD system as a reference, this class will be almost entirely overwritten to begin with'''

class Reference:

    def __init__(self, dt, length, history_func, output_func):
        self.dt = dt
        self.length = length
        self.num_timesteps = int(length / dt)
        self.times = [dt * i for i in range(self.num_timesteps-1)]
        self.history_func = history_func
        self.output = output_func
        #History is the full state description [(pos, vel)].
        #Output is some function of the system history.
        self.history = np.array([history_func(t) for t in self.times])
        self.outputs = np.array([output_func(s) for s in self.history])

    def solve(self, length):
        self.length = length
        self.num_timesteps = int(length / self.dt)
        self.times = np.linspace(0.0, length, num=np.round(length/self.dt)+1)

        self.history = np.array([self.history_func(t) for t in self.times])
        self.outputs = np.array([self.output(s) for s in self.history])

        return self.outputs

    def reset(self):
        pass

    def update(self, dt=None, length=None, history_func=None, output_func=None):

        if dt:
            self.dt = dt

        if length:
            self.length = length

        self.num_timesteps = int(length / dt)
        self.times = [dt * i for i in range(self.num_timesteps)]

        if history_func:
            self.history_func = history_func

        if output_func:
            self.output = output_func

    def plot_pos(self, axes=None, show=False, offset=0, history=False, color='-b'):
        #Plot the position of the system

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        axes.plot(self.times[offset:], self.history[offset:, 0], color)

        axes.set_ylabel('Position', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_vel(self, axes=None, show=False, offset=0, history=False,color='-b'):
        #Plot the velocity of the system

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        axes.plot(self.times[offset:], self.history[offset:, 1], color)

        axes.set_ylabel('Velocity', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_state(self, axes=None, show=False, offset=0, history=False, color='-b'):
        #Plot the state-space of the system

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        axes.plot(self.history[offset:, 0], self.history[offset:, 1], color)

        axes.set_ylabel('Velocity', fontsize=12)
        axes.set_xlabel('Position', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_output(self, axes=None, show=False, offset=0, history=False, color='-b'):

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        axes.plot(self.times[offset:], self.outputs[offset:], color)

        axes.set_ylabel('System Output', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def write_metadata(self):

        meta=dict()
        return meta

'''
The environment class is responsible for connecting the target and reference system together. 
It is responsible for calculating costs, storing traces and plotting output
'''
class MSD_Environment:

    def __init__(self, target, reference, cost, obstacles=None):

        self.target = target
        self.dt = target.dt
        self.ep_length = 10.0
        
        self.num_weights = target.num_weights[0]
        self.weights = target.weights
        self.reference = reference
        self.cost = cost
        self.rollouts = []
        self.obstacles = obstacles

        if obstacles:
            self.target.obstacles = obstacles

        self.reference.reset()
        self.reference_output = self.reference.solve(self.ep_length)

    def change_basis(self, sys, basis, new_basis):
        '''Change a basis type in the given system to the new basis'''

        self.__dict__[sys].change_basis(basis, new_basis)

        self.num_weights = self.target.num_weights[0]
        self.weights = self.target.weights
        self.update_reference()


    def update(self, weights):
        '''Update the weights used by the target system'''

        self.target.update(weights)
        self.weights = self.target.weights

    def update_reference(self):
        self.reference.reset()
        self.reference_output = self.reference.solve(self.ep_length)


    def episode(self):

        '''Perform a single episode'''
        '''Default initial conditions will be used unless specified'''

        #Reset both systems
        self.target.reset()

        #Solve and calculate output
        self.target_output = self.target.solve(self.ep_length)
        self.times = self.target.time_history

        #Calculate cost
        cost = self.cost.get_cost(self.target_output, self.reference_output, obstacles=self.target.obstacles, actions=self.target.external_actions)

        self.reward = cost[0]
        self.reward_list = cost[1]

        self.rollouts.append(Rollout(self))

        return self.reward

    def run(self, length=5.0):
        '''Similar to an episode, but without resetting the system'''

        self.target_output = self.target.solve(length)

        cost = self.cost.get_cost(self.target_output, self.reference_output)

        self.reward = cost[0]
        self.reward_list = cost[1]

    def reset(self):
        '''Reset both systems'''
        self.target.reset()

    def clear_rollouts(self):
        '''Clear history of rollouts'''

        self.rollouts = []

    def perturb(self, max_changes):

        '''Perturb the target weights'''
        self.target.perturb(max_changes)
        self.weights = self.target.weights

    def random_weights(self):

        self.target.random_weights()
        self.weights = self.target.weights


    def plot(self, fig=None, show=False, history=False):

        if not fig:
            fig = plt.figure()
            show = True

        fig.clear()

        axes = fig.add_subplot(2, 4, 1)
        self.target.plot_pos(axes, history=history)
        axes = fig.add_subplot(2, 4, 2)
        self.target.plot_vel(axes, history=history)
        axes = fig.add_subplot(2, 4, 3)
        self.target.plot_state(axes, history=history)
        axes = fig.add_subplot(2, 4, 4)
        self.plot_outputs(axes, history=history)

        axes = fig.add_subplot(2, 4, 5)
        self.reference.plot_pos(axes, history=history, color='-r')
        axes = fig.add_subplot(2, 4, 6)
        self.reference.plot_vel(axes, history=history, color='-r')
        axes = fig.add_subplot(2, 4, 7)
        self.reference.plot_state(axes, history=history, color='-r')
        axes = fig.add_subplot(2, 4, 8)
        self.target.plot_control(axes)

        fig.tight_layout()


        if show:
            fig.show()

    def plot_outputs(self, axes=None, show=False, history=False):

        if not axes:
            fig = plt.figure()
            show = True
            axes = fig.add_subplot(1, 2, 1)

        self.target.plot_output(axes, history=history)
        self.reference.plot_output(axes, history=history, color='-r')

        if show:
            fig.show()
            fig.tight_layout()

    def plot_cost(self, axes=None, show=False):

        if not axes:
            fig = plt.figure()
            show=True
            axes = fig.add_subplot(1, 1, 1)

        axes.plot(self.times, self.reward_list, 'g')
        #axes.plot(self.times, self.cost.control_cost[1], 'r')

        if show:
            fig.tight_layout()
            fig.show()

    def make_param_panel(self, parent):

        return MSD_Panel(parent)

    def plot_basis(self, panel=None):
        if not panel:
            fig = plt.figure()
        else:
            fig = panel.fig

        ax = fig.add_subplot(2, 2, 1)
        self.target.feedback1.plot(ax)
        ax = fig.add_subplot(2, 2, 2, projection='3d')
        self.target.feedback2.plot(ax)
        ax = fig.add_subplot(2, 2, 3)
        self.target.feedback1.plot_features(ax)
        ax = fig.add_subplot(2, 2, 4, projection='3d')
        self.target.feedback2.plot_features(ax)

        if panel:
            panel.canvas.draw()
        else:
            fig.show()

'''This class is a wxPanel used to display/alter environment parameters'''

class MSD_Panel(wx.lib.scrolledpanel.ScrolledPanel):
    def __init__(self, parent):
        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Enviroment Parameters", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.dt_label = wx.StaticText(self, -1, label="Timestep")
        self.output_label = wx.StaticText(self, -1, label="Output")
        self.obstacle_label = wx.StaticText(self, -1, label="Obstacle")

        self.dt = wx.TextCtrl(self, -1)
        self.output = wx.TextCtrl(self, -1)
        self.make_obstacles()

        self.cost_panel = costs.CostPanel(self, self.parent.environment.cost)

        self.episode_length = wx.TextCtrl(self, -1)
        self.episode_length_label = wx.StaticText(self, -1, "Episode Length")
        self.episode_length.SetValue(str(10.0))
        self.tar_init_pos = wx.TextCtrl(self, -1)
        self.tar_init_vel = wx.TextCtrl(self, -1)
        self.tar_init_label = wx.StaticText(self, -1, "Initial Conditions (Target)")
        self.tar_init_pos.SetValue(str(self.parent.environment.target.init_pos))
        self.tar_init_vel.SetValue(str(self.parent.environment.target.init_vel))
        self.obstacle_input_label = wx.StaticText(self, -1, "Obstacle Params")

        self.plot_environment_button = wx.Button(self, -1, "Plot Environment")

        self.Bind(wx.EVT_BUTTON, self.on_plot_env_button, self.plot_environment_button)

        self.update_button = wx.Button(self, -1, "Update Enviroment")
        self.plot_basis_button = wx.Button(self, -1, "Plot Basis Functions")
        self.draw_traj_button = wx.Button(self, -1, "Draw Reference Trajectory")

        self.obstacle_input = wx.TextCtrl(self, -1)

        self.add_obstacle_button = wx.Button(self, -1, "Add Obstacle")
        self.del_obstacle_button = wx.Button(self, -1, "Delete Obstacle")
        self.Bind(wx.EVT_BUTTON, self.on_del_obstacle, self.del_obstacle_button)
        self.Bind(wx.EVT_BUTTON, self.on_add_obstacle, self.add_obstacle_button)

        self.target_stiffness = BasisPanel(self, 'target', 'stiffness')
        self.target_damping = BasisPanel(self, 'target', 'damping')
        self.target_feedback1 = BasisPanel(self, 'target', 'feedback1')
        self.target_feedback2 = BasisPanel(self, 'target', 'feedback2')
        self.target_impulse = BasisPanel(self, 'target', 'impulse')
        self.target_controller = BasisPanel(self, 'target', 'external')

        self.Bind(wx.EVT_BUTTON, self.on_update, self.update_button)
        self.Bind(wx.EVT_BUTTON, self.on_plot_basis, self.plot_basis_button)
        self.Bind(wx.EVT_BUTTON, self.on_draw_traj, self.draw_traj_button)

        self.sizer.Add(self.title, (0, 0), flag=wx.ALL)
        self.sizer.Add(self.dt_label, (1, 0), flag=wx.EXPAND)
        self.sizer.Add(self.output_label, (2, 0), flag=wx.EXPAND)
        self.sizer.Add(self.obstacle_label, (3, 0), flag=wx.EXPAND)

        self.sizer.Add(self.dt, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.output, (2, 1), flag=wx.EXPAND)

        self.sizer.Add(self.episode_length_label, (4, 0), flag=wx.EXPAND)
        self.sizer.Add(self.tar_init_label, (5, 0), flag=wx.EXPAND)
        self.sizer.Add(self.episode_length, (4, 1), flag=wx.EXPAND)

        self.tar_init_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.tar_init_sizer.Add(self.tar_init_pos, flag=wx.EXPAND)
        self.tar_init_sizer.Add(self.tar_init_vel, flag=wx.EXPAND)
        self.sizer.Add(self.tar_init_sizer, (5, 1),  flag=wx.EXPAND)

        self.sizer.Add(self.obstacle_input_label, (7, 0), flag=wx.EXPAND)
        self.sizer.Add(self.obstacle_input, (7, 1), span=(1, 1), flag=wx.EXPAND)

        self.sizer.Add(self.del_obstacle_button, (8, 0), flag=wx.EXPAND)
        self.sizer.Add(self.add_obstacle_button, (8, 1), flag=wx.EXPAND)
        self.sizer.Add(self.plot_environment_button, (9, 0), flag=wx.EXPAND)


        self.sizer.Add(self.update_button, (9, 1), flag = wx.EXPAND)
        self.sizer.Add(self.plot_basis_button, (10, 0), flag = wx.EXPAND)
        self.sizer.Add(self.draw_traj_button, (10, 1), flag = wx.EXPAND)

        self.sizer.Add(self.cost_panel, (11, 0), span = (1, 2), flag=wx.EXPAND)

        self.sizer.Add(self.target_stiffness, (13, 0), flag=wx.EXPAND)
        self.sizer.Add(self.target_damping, (13, 1), flag=wx.EXPAND)
        self.sizer.Add(self.target_feedback1, (14, 0), flag=wx.EXPAND)
        self.sizer.Add(self.target_feedback2, (14, 1), flag=wx.EXPAND)
        self.sizer.Add(self.target_impulse, (15, 0), flag=wx.EXPAND)
        self.sizer.Add(self.target_controller, (15, 1), flag=wx.EXPAND)
        self.get_params()

        self.ref_init_pos = wx.TextCtrl(self, -1)
        self.ref_init_vel = wx.TextCtrl(self, -1)

        try:
            self.ref_stiffness = BasisPanel(self, 'reference', 'stiffness')
            self.ref_damping = BasisPanel(self, 'reference', 'damping')
            self.sizer.Add(self.ref_stiffness, (12, 0), flag=wx.EXPAND)
            self.sizer.Add(self.ref_damping, (12, 1), flag=wx.EXPAND)
            self.ref_init_pos.SetValue(str(self.parent.environment.reference.init_pos))
            self.ref_init_vel.SetValue(str(self.parent.environment.reference.init_vel))
        except:
            pass

        self.ref_init_label = wx.StaticText(self, -1, "Initial Conditions (Reference)")
        self.ref_init_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.ref_init_sizer.Add(self.ref_init_pos, flag=wx.EXPAND)
        self.ref_init_sizer.Add(self.ref_init_vel, flag=wx.EXPAND)
        self.sizer.Add(self.ref_init_label, (6, 0), flag=wx.EXPAND)
        self.sizer.Add(self.ref_init_sizer, (6, 1),  flag=wx.EXPAND)

        self.SetSizer(self.sizer)
        self.Layout()

    def make_obstacles(self):

        if hasattr(self, 'obstacle'):
            self.obstacle.Destroy()

        obstacles = ["T:(%.2f, %.2f), R:(%.2f, %.2f), W:%.2f" % (o.timesteps[0], o.timesteps[1], o.region[0], o.region[1], o.weight) for o in self.parent.environment.target.obstacles]
        self.obstacle = wx.ComboBox(self, -1, choices=obstacles)
        self.sizer.Add(self.obstacle, (3, 1), flag=wx.EXPAND)
        self.Layout()

    def on_add_obstacle(self, event):

        obj_string = [float(_) for _ in self.obstacle_input.GetValue().split(',')]
        obj = Obstacle((obj_string[0], obj_string[1]), (obj_string[2], obj_string[3]), obj_string[4])

        self.parent.environment.target.obstacles.append(obj)
        self.make_obstacles()

    def on_del_obstacle(self, event):

        if self.parent.environment.target.obstacles:
            index = self.obstacle.GetSelection()
            try:
                del self.parent.environment.target.obstacles[index]
            except:
                pass

        self.make_obstacles()

    def get_params(self):

        self.dt.SetValue(str(self.parent.environment.target.dt))
        self.output.SetValue(self.parent.environment.target.output_type)

    def on_plot_basis(self, event):
        self.parent.environment.plot_basis(self.parent.EnvPlot)

    def on_draw_traj(self, event):
        
        line_t = [0.]
        line_x = [0.]

        fig = plt.figure()
        fig_num = fig.number 

        ax = plt.gca()
        ax.plot(line_t, line_x)
        ax.set_xlim([0, max(2*max(line_t), 1.0)])
        ax.set_ylim([min(-2*max(line_x), -1.0), max(2*max(line_x), 1.0)])
        plt.draw()

        while plt.fignum_exists(fig_num):

            try:
                (t, x) = plt.ginput(1)[0]
                if t > line_t[-1]:
                    line_t.append(t)
                    line_x.append(x)

                ax = plt.gca()
                ax.plot(line_t, line_x)
                ax.set_xlim([0, max(2*max(line_t), 1.0)])
                ax.set_ylim([min(-2*max(line_x), -1.0), max(2*max(line_x), 1.0)])
                plt.draw()
            except:
                pass


    def on_update(self, event):

        ep_length = float(self.episode_length.GetValue())
        tar_init_pos = float(self.tar_init_pos.GetValue())
        tar_init_vel = float(self.tar_init_vel.GetValue())

        ref_init_pos = float(self.ref_init_pos.GetValue())
        ref_init_vel = float(self.ref_init_vel.GetValue())

        self.parent.environment.ep_length = ep_length
        self.parent.environment.target.init_pos = tar_init_pos
        self.parent.environment.target.init_vel = tar_init_vel

        try:
            self.parent.environment.reference.init_pos = ref_init_pos
            self.parent.environment.reference.init_vel = ref_init_vel
        except:
            pass

    def on_plot_env_button(self, event):

        if self.parent.environment:
            self.parent.environment.episode()
            self.parent.environment.plot(self.parent.EnvFig)
            self.parent.EnvCanvas.draw()

        else:
            self.parent.write("Please load a system first")

class BasisPanel(wx.Panel):

    def __init__(self, parent, sys, b):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)

        self.parent = parent
        self.arg_string = [sys, b]
        self.system = self.parent.parent.environment.__dict__[sys]
        self.basis = self.parent.parent.environment.__dict__[sys].__dict__[b]

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label=sys + ': ' + b, style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.name_label = wx.StaticText(self, -1, label="Name")
        self.num_weights_label = wx.StaticText( self, -1, label="Num weights")
        self.weights_label = wx.StaticText(self, -1, label="Weights")

        BASIS_LIST = [a for a in dir(basis) if isinstance(basis.__dict__.get(a), types.ClassType)]
        
        self.name = wx.ComboBox(self, -1, choices=BASIS_LIST)
        self.name.isEditable = False
        self.name.SetValue(self.basis.type)
        self.num_weights = wx.TextCtrl(self, -1)
        self.num_weights.SetValue(str(self.basis.num_weights))
        self.weights = wx.TextCtrl(self, -1)
        self.weights.SetValue(str(["%.2f" % w for w in self.basis.weights]))

        self.update_button = wx.Button(self, -1, label="Edit Weights")
        self.random_button = wx.Button(self, -1, label="Randomise weights")
        self.change_basis_button = wx.Button(self, -1, label="Change Basis")

        self.sizer.Add(self.title, (0, 0), flag=wx.EXPAND)
        self.sizer.Add(self.name_label, (1, 0), flag=wx.EXPAND)
        self.sizer.Add(self.num_weights_label, (2, 0), flag=wx.EXPAND)
        self.sizer.Add(self.weights_label, (3, 0), flag=wx.EXPAND)
        self.sizer.Add(self.name, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.num_weights, (2, 1), flag=wx.EXPAND)
        self.sizer.Add(self.weights, (3, 1), flag=wx.EXPAND)

        self.sizer.Add(self.update_button, (4, 0), flag=wx.EXPAND)
        self.sizer.Add(self.random_button, (4, 1), flag=wx.EXPAND)
        self.sizer.Add(self.change_basis_button, (5, 0), flag=wx.EXPAND)

        self.Bind(wx.EVT_BUTTON, self.on_update_weights, self.update_button)
        self.Bind(wx.EVT_BUTTON, self.on_random, self.random_button)
        self.Bind(wx.EVT_BUTTON, self.on_change_basis, self.change_basis_button)
        self.SetSizer(self.sizer)
        self.Layout()

    def on_change_basis(self, event):

        sys, b = self.arg_string
        num_weights = int(self.num_weights.GetValue())
        new_basis = basis.__dict__.get(self.name.GetValue())
        self.parent.parent.environment.change_basis(sys, b, new_basis(num_weights = num_weights))
        self.basis = self.parent.parent.environment.__dict__[sys].__dict__[b]
        self.weights.SetValue(str(["%.2f" % w for w in self.basis.weights]))

    def on_update_weights(self, event):

        new_weights = [float(w[1:-1]) for w in self.weights.GetValue().strip('[]').replace(' ', '').split(',')]

        if len(new_weights) == self.basis.num_weights:
            sys, b = self.arg_string
            self.parent.parent.environment.__dict__[sys].__dict__[b].update(new_weights)
        else:
            self.parent.parent.write("Incorrect Number of weights")


    def on_random(self, event):
        
        self.basis.random_weights()
        self.weights.SetValue(str(["%.2f" % w for w in self.basis.weights]))