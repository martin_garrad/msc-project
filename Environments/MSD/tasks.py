import msd_environment as environment
import msd
import costs
import basis
import numpy as np
import random
from scipy import signal

'''
This section contains a number of pre-defined environments
'''
def ReachingStiffness():

	ref = msd.ReachingTask1()
	ref.init_pos = 2.0

	tar = msd.Stiffness()
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	tar.damping = ref.damping
	tar.init_pos = 2.0

	env = environment.MSD_Environment(tar, ref, c)

	return env

def ReachingDamping():

	ref = msd.ReachingTask2()
	ref.init_pos = 2.0

	tar = msd.CubicDamping()
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	tar.stiffness = ref.stiffness
	tar.init_pos = 2.0

	env = environment.MSD_Environment(tar, ref, c)

	return env

def ReachingStiffnessDamping():

	ref = msd.ReachingTask3()
	tar = msd.StiffDamp()
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)
	ref.init_pos = 2.0
	tar.init_pos = 2.0

	env = environment.MSD_Environment(tar, ref, c)

	return env

def LocomotionTask():

	ref = msd.LocomotionTask1()

	tar = msd.Leonard()

	tar.stiffness = ref.stiffness
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def LocomotionTaskStiffness():

	ref = msd.LocomotionTask1()
	tar = msd.StiffLeonard()

	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def Step():

	dt = 0.001
	length = 10.0
	h_func = lambda t: np.array([-1.0, 0.0]) if t < 5.0 else np.array([1.0, 0.0])
	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.OddPolyLin()
	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def FixedFeedback():
	'''This task has a fixed PD controller. The morphology acts as an adaptive open-loop controller
	which can be adjusted to minimise the work done by the external controller
	'''

	ref = msd.ReachingTask1()
	ref.init_pos = 2.0
	ref.reset()
	ref.solve(10.0)

	tar = msd.OddPolyLin(external=basis.PD(ref, 0))
	tar.init_pos = 2.0

	c = costs.Cost([0.0, 0.0, 1.0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def OnlyFeedback():
	'''This task has a fixed morphology and aims to minimise the feedback gains
	'''

	ref = msd.ReachingTask1()
	ref.init_pos = 2.0
	ref.reset()
	ref.solve(10.0)

	tar = msd.System()
	tar.external=basis.PD(ref, 2)
	tar.init_pos = 2.0

	c = costs.Cost([0.0, 0.0, 1.0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def FullFeedback():
	'''This task has a fixed morphology and aims to minimise the feedback gains
	'''

	ref = msd.ReachingTask1()
	ref.init_pos = 2.0
	ref.reset()
	ref.solve(10.0)

	tar = msd.OddPolyLin(external=basis.PD(ref, 2))
	tar.init_pos = 2.0

	c = costs.Cost([0.0, 0.0, 1.0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def GaussianStiffness():
	'''
	This task uses an open-loop DMP style external
	signal to solve the ReachingStiffness task
	'''
	ref = msd.ReachingTask3()
	tar = msd.OddPolyLin(external=basis.Gaussian(10))
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def GaussianOnly():

	ref = msd.ReachingTask1()
	tar = msd.System()
	tar.external = basis.Gaussian(10)
	tar.init_pos = 2.0
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def DMPStiffness():
	'''
	This task uses an open-loop DMP style external
	signal to solve the ReachingStiffness task
	'''
	ref = msd.ReachingTask1()
	params = dict()
	params['damping'] = basis.LinearDamping(num_weights=1)
	params['stiffness'] = basis.Null()
	params['feedback1'] = basis.OddPoly(num_weights=2)
	params['external'] = basis.DMP(10)
 	tar = msd.System(params)
 	tar.damping = ref.damping
	tar.init_pos = 2.0

	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def DMPOnly():

	ref = msd.ReachingTask1()
	params = dict()
	params['feedback1'] = basis.Null()
	params['feedback2'] = basis.Null()
	params['stiffness'] = basis.OddPoly(num_weights=1)
	params['external'] = basis.DMP(10)
 	tar = msd.System(params)
	tar.init_pos = 2.0
	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env


def DMPFixedStep():

	'''
	This task requires the agent to adapt the morphology to optimise the tracking performance.
	The DMP control signal is fixed.
	'''
	dt = 0.001
	length = 10.

	def h_func(t):
		if t < 1.0:
			return np.array([0.0, 0.])
		elif t < 5.0:
			return np.array([-1.0, 0.])
		else:
			return np.array([1.0, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.OddPolyLin(external=basis.DMP(0, weights = [10., 30., -10., 30., 30., 50., 30., 0., -50., -50.]))
	tar.init_pos = 0.0
	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def DMPOnlyStep():

	'''
	This task requires the agent to adapt the DMP signal to optimise the tracking performance.
	The morphology is fixed.
	'''
	dt = 0.001
	length = 10.

	def h_func(t):
		if t < 1.0:
			return np.array([0.0, 0.])
		elif t < 5.0:
			return np.array([-1.0, 0.])
		else:
			return np.array([1.0, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	params = dict()
	params['damping'] = basis.LinearDamping(num_weights=1)
	params['stiffness'] = basis.OddPolyLin(num_weights=2)
	params['feedback1'] = basis.Null()
	params['external'] = basis.DMP(10, weights = [10., 30., -10., 30., 30., 50., 30., 0., -50., -50.])
 	tar = msd.System(params)
	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def DMPStep():

	'''
	This task requires the agent to adapt both the DMP signal and morphology to optimise tracking performance.
	'''
	dt = 0.001
	length = 10.

	def h_func(t):
		if t < 1.0:
			return np.array([0.0, 0.])
		elif t < 5.0:
			return np.array([-1.0, 0.])
		else:
			return np.array([1.0, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.OddPolyLin(external=basis.DMP(10, weights = [20., 20., 20., 20., 20., -20., -20., -20., -20., -20.]))
	tar.init_pos = 0.0
	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def LinearStiffness():

	'''
	This task requires the agent to adapt the morphology to optimise the tracking performance.
	The reference signal is a simple linear decay of fixed (random) gradient. 
	'''
	dt = 0.01
	length = 10.
	pos = 2.0

	bottom_t = 2

	grad1 = (2.0) / bottom_t

	def h_func(t):
		if t < bottom_t:
			pos = 2.0 - (t * grad1)
			return np.array([pos, 0.])

		else:
			return np.array([0.0, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.StiffDamp()
	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def LinearStiffnessObstacle():

	'''
	This task requires the agent to adapt the morphology to optimise the tracking performance.
	The reference signal is a simple linear decay of fixed (random) gradient. 
	'''
	dt = 0.01
	length = 10.
	pos = 2.0

	zero_cross = 3.1
	grad = 2.0 / zero_cross

	def h_func(t):
		if t < zero_cross:
			pos = 2.0 - (t * grad)
			return np.array([pos, 0.])
		else:
			return np.array([0.0, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.StiffLinObs()
	c = costs.Cost([0, 1, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def SineLimit():

	dt = 0.01
	length = 10.
	amp = 1.5
	freq = 2

	def h_func(t):

		x = amp*np.sin(freq*t + np.pi/2)
		return np.array([x, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.StiffLeonard()

	tar.init_pos = amp

	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env

def SquareLimit():

	dt = 0.01
	length = 10.
	amp = 1.5
	freq = 1.5

	def h_func(t):

		x = amp*signal.sawtooth(freq*t + np.pi, 0.5)
		return np.array([x, 0.])

	o_func = lambda s: [s[0]]

	ref = environment.Reference(dt, length, h_func, o_func)
	tar = msd.StiffLeonard()

	tar.init_pos = amp

	c=costs.Cost([0, 0, 0, 1.0], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env



def Default():

	ref = msd.ReachingTask1()
	tar = msd.Stiffness()

	c = costs.Cost([0, 0, 0, 1], tar.dt, 10.0)

	env = environment.MSD_Environment(tar, ref, c)

	return env




