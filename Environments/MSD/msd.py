#!/usr/bin/env python
import numpy as np
import matplotlib as mpl 
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import seaborn as sn
import basis
import output
import copy
import math
import wx
import msd_environment

'''A simple mass-spring-damper simulation
These parameters are default parameters that can be overwritten
by passing in a dict to the class constructor'''
MASS = 10.0
STIFFNESS = basis.OddPoly([0])
DAMPING = basis.LinearDamping([0])
EXTERNAL = lambda t: 0.0
INIT_POS = 2.0
INIT_VEL = 0.0
DT = 0.01

'''
These parameters are used when creating systems with randomised parameters
'''
MIN_STIFFNESS = 1
MAX_STIFFNESS = 100
MIN_CUBIC_STIFFNESS = 1
MAX_CUBIC_STIFFNESS = 10
MIN_DAMPING = 1.0
MAX_DAMPING = 20.0
MIN_SIZE = 0.1
MAX_SIZE = 2
MAX_NONLINEARITY = 10
ODD_POLY_WEIGHTS = 2
TAN_SIG_WEIGHTS = 5
LEONARD_WEIGHTS = 2
DMP_WEIGHTS = 5

'''Simple PD controller for experimenation with control minimisation
Eventually this should probably move into a separate file?
'''

'''This class represents the mass-spring-damper system'''

class System:

    def __init__(self, params=dict()):

        #Create default values - these will be overwritten if specified in params
        self.mass = MASS
        self.stiffness = STIFFNESS
        self.damping = DAMPING
        self.external = basis.Null()
        self.feedback1 = basis.OddPoly([1])
        self.feedback2 = basis.Null()
        self.impulse = basis.Null()
        self.output = output.pos_output()[0]
        self.output_type = output.pos_output()[1]
        self.init_pos = INIT_POS
        self.init_vel = INIT_VEL
        self.name = "Default System"
        self.dt = DT
        #Gaussian actions are used to create exploration in policy gradient methods
        self.gaussian_actions = True
        self.variance = 0.05
        #Obstacles is a list of regions to avoid
        self.obstacles = []

        #Overwrite defaults if given parameters
        for (k, v) in params.iteritems():
            setattr(self, k, v)

        #Initalise storage
        self.next_pos = self.init_pos
        self.next_vel = self.init_vel
        self.history = []
        self.actions = []
        #External actions is the input provided by any external controller
        self.external_actions = []
        '''
        Activations is a list of lists. 
        Each sub-list corresponds to the basis function activations at a particular timestep
        '''
        self.activations = []
        self.stiffness_activations = []
        self.damping_activations = []
        self.impulse_activations = []
        self.controller_activations = []
        '''
        Means represents the mean value of the basis functions. The policy is a gaussian centred on the mean value.
        '''
        self.means = []
        self.variances = []
        self.output_history = []
        self.time_history = []
        self.hit_obstacle = []

        #Num weights is a list. The first index contains the total number of weights, 
        #with the following indices representing the points at which the weights should be split for the basis functions
        self.num_weights = [self.feedback1.num_weights + self.feedback2.num_weights + self.impulse.num_weights + self.external.num_weights, self.feedback1.num_weights, self.feedback2.num_weights, self.impulse.num_weights, self.external.num_weights]
        self.weights = list(self.feedback1.weights) + list(self.feedback2.weights) + list(self.impulse.weights) + list(self.external.weights)

    def change_basis(self, basis_name, new_basis):

        #Update one of the basis functions for this system
        self.__dict__[basis_name] = new_basis

        self.num_weights = [self.feedback1.num_weights + self.feedback2.num_weights + self.impulse.num_weights + self.external.num_weights, self.feedback1.num_weights, self.feedback2.num_weights, self.impulse.num_weights, self.external.num_weights]
        self.weights = list(self.feedback1.weights) + list(self.feedback2.weights) + list(self.impulse.weights) + list(self.external.weights)

    def update(self, weights):

        '''
        This function updates the weights of the basis functions
        The number of weights must not change during a simulation
        '''

        assert len(weights) == self.num_weights[0], "Number of weights must be: %d (Given %d)" % (self.num_weights[0], len(weights))
        
        self.feedback1.update(weights[0:self.num_weights[1]])
        self.feedback2.update(weights[self.num_weights[1]:self.num_weights[1]+self.num_weights[2]])
        self.impulse.update(weights[self.num_weights[1]+self.num_weights[2]:self.num_weights[1]+self.num_weights[2]+self.num_weights[3]])
        self.external.update(weights[self.num_weights[1]+self.num_weights[2]+self.num_weights[3]:])

        self.weights = list(self.feedback1.weights) + list(self.feedback2.weights) + list(self.impulse.weights) + list(self.external.weights)

    def perturb(self, max_changes):

        '''
        This function makes a small random change to each of the weights
        '''

        self.feedback1.perturb(max_changes[0:self.num_weights[1]])
        self.feedback2.perturb(max_changes[self.num_weights[1]:self.num_weights[2]])

        if self.impulse.num_weights == 1:
            self.impulse.perturb(max_changes[-1])

        #if self.external:
            #self.external.perturb()

        self.weights = list(self.feedback1.weights) + list(self.feedback2.weights) + list(self.impulse.weights) + list(self.external.weights)

    def reset(self):

        #Reset to initial state

        self.next_pos = self.init_pos
        self.next_vel = self.init_vel
        self.history = []
        self.actions = []
        self.external_actions = []
        self.activations = []
        self.stiffness_activations = []
        self.damping_activations = []
        self.impulse_activations = []
        self.controller_activations = []
        self.means = []
        self.output_history = []
        self.time_history = []
        self.variances = []
        self.external.reset()

    def grad(self, z, t):

        '''
        This function calculates the gradient of the system for a given state
        It also stores the necessary information for any learning agents to access
        '''

        #Calculate applied forces
        inherent_stiffness = self.stiffness.func(z[0])
        stiffness_action = self.feedback1.func(z[0])
        inherent_damping = self.damping.func(z)
        damping_action = self.feedback2.func(z)

        impulse_action = self.impulse.func(t)
        controller_action = self.external.func((z, t))

        #Combine feedback forces and store

        feedback_action = stiffness_action + damping_action
        external_action = impulse_action + controller_action

        '''
        print "State is [%f, %f]" % (z[0], z[1])
        print "Stiffness is %f" % stiffness_action
        print "Damping is %f" % damping_action
        print "Total Feedback is %f" % feedback_action
        print "External is %f" % external_action
        '''
        
        policy_action = feedback_action + external_action
        self.means.append(policy_action)

        #If policy is guassian, sample action
        if self.gaussian_actions:
            #Ensure variance is of reasonable size
            variance = self.variance
            self.variances.append(variance)
            noise = np.random.normal(0, variance)

        '''
        print "Policy action is %f" % policy_action
        print "Noise is %f" % noise
        print ''
        '''

        policy_action += noise
        #Store feedback force
        self.actions.append(-policy_action)
        self.external_actions.append(-external_action)

        #Combine inherent and feedback forces
        total_action = policy_action + inherent_damping + inherent_stiffness

        #Store activations of each basis function
        stiffness_activations = self.feedback1.activations(z[0])
        damping_activations = self.feedback2.activations(z)
        impulse_activations = self.impulse.activations(t)
        controller_activations = self.external.activations((z, t))
        activations = stiffness_activations + damping_activations + impulse_activations + controller_activations

        self.stiffness_activations.append(stiffness_activations)
        self.damping_activations.append(damping_activations)
        self.impulse_activations.append(impulse_activations)
        self.controller_activations.append(controller_activations)
        self.activations.append(activations)

        #Return state-space gradient
        return np.array(
        [z[1],
        (-total_action)/self.mass]
        )

    def integrate(self, state, times):

        '''Simple Newton-Euler integration '''

        states = []

        for t in times:
            states.append(copy.deepcopy(state))
            gradient = self.grad(state, t)
            state += (self.dt * gradient)

        return np.array(states)

    def rk4_integrate(self, state, times):

        #Not used currently - need to ensure this is correct w.r.t randomly sampled action.

        states = []
        '''
        for t in times:
            states.append(copy.deepcopy(state))

            k1 = self.grad(state, t) * self.dt 
            self.gaussian_actions = False
            k2 = self.grad(state+k1/2., t+(self.dt/2.)) * self.dt
            k3 = self.grad(state+k2/2., t+(self.dt/2.)) * self.dt
            k4 = self.grad(state+k3, t+self.dt) * self.dt
            self.gaussian_actions = True

            state += k1/6. + k2/3. + k3/3. + k4/6.

        return np.array(states)
        '''

    def solve(self, t):
        #Solve for a set period of time

        #Set initial values
        z0 = np.array([self.next_pos, self.next_vel])
            
        #Store time series            
        self.times = np.linspace(0.0, t, num=np.round(t/self.dt)+1)

        #Solve system and calculate output / reference signal
        self.state = self.integrate(z0, self.times)

        #Calculate output
        self.outputs = np.array(map(self.output, self.state))

        #Store state and output history
        if self.history == []:
            self.history = self.state
            self.output_history = self.outputs
            self.time_history = self.times
        else:
            np.concatenate((self.history, self.state))
            np.concatenate((self.output_history, self.outputs))
            np.concatenate((self.time_history, self.times))

        #Set next state to last state in solution
        self.next_pos = self.state[-1, 0]
        self.next_vel = self.state[-1, 1]

        return self.outputs

    def plot_pos(self, axes=None, show=False, offset=0, history=False, color='-b'):
        #Plot the position of the system

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.history[offset:, 0], color)
        else:
            axes.plot(self.times[offset:], self.state[offset:, 0], color)

        if self.obstacles:
            for obstacle in self.obstacles:
                axes.add_patch(Rectangle((obstacle.timesteps[0], obstacle.region[0]), obstacle.timesteps[1]-obstacle.timesteps[0], obstacle.region[1]-obstacle.region[0], facecolor="gray"))

        axes.set_ylabel('Position', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_vel(self, axes=None, show=False, offset=0, history=False,color='-b'):
        #Plot the velocity of the system

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.history[offset:, 1], color)
        else:
            axes.plot(self.times[offset:], self.state[offset:, 1], color)
        axes.set_ylabel('Velocity', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_state(self, axes=None, show=False, offset=0, history=False, color='-b'):
        #Plot the state-space of the system

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.history[offset:, 0], self.history[offset:, 1], color)
        else:
            axes.plot(self.state[offset:, 0], self.state[offset:, 1], color)
        axes.set_ylabel('Velocity', fontsize=12)
        axes.set_xlabel('Position', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_output(self, axes=None, show=False, offset=0, history=False, color='-b'):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.output_history[offset:], color)
        else:
            axes.plot(self.times[offset:], self.outputs[offset:], color)
        axes.set_ylabel('System Output', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()

    def plot_field(self, axes=None, show=False):
        
        gaussian_actions = copy.deepcopy(self.gaussian_actions)
        self.gaussian_actions = False
        if not axes:
            fig = plt.figure()
            show=True
            axes = fig.add_subplot(1, 1, 1)

        X, Y = np.mgrid[-5:5:20j, -5:5:20j]
        U = self.grad([X, Y], .1)[0]
        V = self.grad([X, Y], .1)[1]

        axes.quiver(X, Y, U, V, pivot='mid')
        self.gaussian_actions = gaussian_actions

        if show:
            fig.show()

    def plot_stream(self, axes=None, show=False):
        
        if not axes:
            fig = plt.figure()
            show=True
            axes = fig.add_subplot(1, 1, 1)

        x = np.linspace(-5, 5, 200)
        y = np.linspace(5, -5, 200)
        u = self.grad([x, y], 1)[0]
        v = self.grad([x, y], 1)[1]
        U, V = np.meshgrid(u, v)
        axes.streamplot(x, y, U, V)

        #Not sure if this is necessary with GUI
        #plt.axis([-12, 12, -12, 12])

        if show:
            fig.show()

    def plot_control(self, axes=None, show=False):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True


        axes.plot(self.times, self.external_actions)
        axes.set_ylabel('Control Signal', fontsize=12)
        axes.set_xlabel('Time(s)', fontsize=12)
        axes.tick_params(axis='both', which='major', labelsize=12)

        if show:
            fig.show()


    def write_metadata(self):
        
        meta = dict()
        meta['mass'] = self.mass
        meta['stiffness'] = self.stiffness.write_metadata()
        meta['damping'] = self.damping.write_metadata()
        meta['feedback1'] = self.feedback1.write_metadata()
        meta['feedback2'] = self.feedback2.write_metadata()
        meta['initial conditions'] = (self.init_pos, self.init_vel)
        meta['time step'] = self.dt
        meta['obstacle'] = self.obstacles

        return meta

    def random_weights(self):
        self.feedback1.random_weights()
        self.feedback2.random_weights()
        #self.impulse.update(weights[self.num_weights[1]+self.num_weights[2]:self.num_weights[1]+self.num_weights[2]+self.num_weights[3]])
        #self.external.update(weights[self.num_weights[1]+self.num_weights[2]+self.num_weights[3]:])

        self.weights = list(self.feedback1.weights) + list(self.feedback2.weights) + list(self.impulse.weights) + list(self.external.weights)


    def make_ref_panel(self):
        pass

    def make_tar_panel(self):
        pass

'''
This section contains a number of pre-defined REFERENCE systems for the agent to learn
'''

def LinearSine():

    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    sys.stiffness = basis.OddPoly(stiffness)
    sys.name = "LinearSine"

    return sys

def NonLinearSine():

    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    cubic_stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    sys.stiffness = basis.OddPoly([stiffness, cubic_stiffness])
    sys.name = "NonLinearSine"

    return sys

def DampedLinearSine():
    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    sys.stiffness = basis.OddPoly([stiffness])

    damping = MIN_DAMPING + (random.random() * MAX_DAMPING)
    sys.damping = basis.LinearDamping([damping])
    sys.name = "DampedLinearSine"

    return sys

def DampedNonLinearSine():

    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    cubic_stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    sys.stiffness = basis.OddPoly([stiffness, cubic_stiffness])

    damping = MIN_DAMPING + (random.random() * MAX_DAMPING)
    sys.damping = basis.LinearDamping([damping])
    sys.name = "DampedNonLinearSine"

    return sys

def LimitCycle():
    
    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    size = MIN_SIZE + (random.random() * MAX_SIZE)
    nonlinearity = MAX_NONLINEARITY * random.random()

    sys.stiffness = basis.OddPoly([stiffness])
    sys.damping = basis.Leonard([-nonlinearity*size, nonlinearity])
    sys.name = "LimitCycle"

    return sys

def NonLinearStiffnessLimitCycle():

    sys = System()
    stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    cubic_stiffness = MIN_STIFFNESS + (random.random() * MAX_STIFFNESS)
    sys.stiffness = basis.OddPoly([stiffness, cubic_stiffness])

    size = MIN_SIZE + (random.random() * MAX_SIZE)
    nonlinearity = MAX_NONLINEARITY * random.random()

    sys.damping = basis.Leonard([-nonlinearity*size, nonlinearity])
    sys.name = "NonLinearStiffnessLimitCycle"

    return sys

def ReachingTask1():

    sys = System()

    stiffness = 5.
    cubic_stiffness = 8.0
    tanh_stiffness1 = 5.0
    tanh_stiffness2 = 8.0
    sys.stiffness = basis.Stiffness([tanh_stiffness1, tanh_stiffness2, stiffness, cubic_stiffness])

    sys.damping = basis.LinearDamping([5.])

    sys.name = "ReachingTask1"

    return sys

def ReachingTask2():

    sys = System()

    stiffness = 4.0
    cubic_stiffness = 12.0
    sys.stiffness = basis.OddPoly([stiffness, cubic_stiffness])

    damping = 4.0
    cubic_damping = 7.0
    sys.damping = basis.CubicDamping([damping, cubic_damping])

    sys.name = "ReachingTask2"

    return sys

def ReachingTask3():

    sys = System()

    stiffness = 5.
    cubic_stiffness = 15.0
    tanh_stiffness1 = 5.0
    tanh_stiffness2 = 18.0
    sys.stiffness = basis.Stiffness([tanh_stiffness1, tanh_stiffness2, stiffness, cubic_stiffness])

    damping = 5.0
    cubic_damping = 2.0
    tanh_damping1 = 5.0
    tanh_damping2 = 4.0
    sys.damping = basis.Damping([tanh_damping1, tanh_damping2, damping, cubic_damping])

    sys.name = "ReachingTask3"

    return sys

def LocomotionTask1():

    sys = System()

    sys.stiffness = basis.Stiffness(num_weights=4)

    sys.damping = basis.Leonard([-5, 10])

    sys.name = "LocomotionTask1"

    return sys

def LocomotionTask2():

    sys = System()

    sys.stiffness = basis.OddPoly([5, 20])

    sys.damping = basis.Leonard([-50, 10])

    sys.name = "LocomotionTask2"

    return sys

def ReachingObstacle():

    sys = ReachingTask1()
    sys.name = "ReachingObstacle"

    return sys

def Controlled(ref):

    sys = System()

    stiffness = 0.0
    cubic_stiffness = 0.0
    sys.stiffness = basis.OddPoly([stiffness, cubic_stiffness])

    damping = 0.0
    sys.damping = basis.LinearDamping([damping])

    sys.external = basis.PD(ref, 0)

    sys.name = "ReachingTask1"

    return sys

'''
This section contains a number of targets which can be used 
by an agent as a basis for learning
'''

def Fixed():

    sys = System()

    return sys

def OddPoly(num_weights=ODD_POLY_WEIGHTS):
    
    params = dict()
    params['feedback1'] = basis.OddPoly(num_weights=num_weights)
    sys = System(params)

    return sys

def TanSig(num_weights=TAN_SIG_WEIGHTS):

    sys = System()
    sys.feedback1 = basis.TanSig(num_weights=num_weights)
    sys.name = "TanSig"

    return sys

def LinDamping():

    sys = System()
    sys.feedback2 = basis.LinearDamping()
    sys.name = "LinDamping"

    return sys

def CubicDamping():

    params = dict()
    params['feedback1'] = basis.Null()
    params['feedback2'] = basis.CubicDamping()

    sys = System(params)

    sys.name = "CubicDamping"

    return sys

def Leonard(num_weights=LEONARD_WEIGHTS):

    params = dict()

    params['feedback2'] = basis.Leonard(num_weights=num_weights)
    sys = System(params)
    sys.name = "Leonard"

    return sys


'''These targets allow both stiffness and damping to be adapted'''
def LinLeonard(num_weights=LEONARD_WEIGHTS):

    params = dict()
    params['feedback1'] = basis.OddPoly(num_weights=1)
    params['feedback2'] = basis.Leonard(num_weights=num_weights)
    params['name'] = "LinLeonard"
    sys = System(params)

    return sys

def TanSigLin(num_weights=TAN_SIG_WEIGHTS):

    params = dict()
    params['feedback1'] = basis.TanSig(num_weights=TAN_SIG_WEIGHTS)
    params['feedback2'] = basis.LinearDamping()
    params['name'] = "TanSigLin"
    sys = System(params)


    return sys

def OddPolyLin(num_weights=ODD_POLY_WEIGHTS, external = basis.Null()):

    params = dict()

    params['feedback1'] = basis.OddPoly(num_weights=ODD_POLY_WEIGHTS)
    params['feedback2'] = basis.LinearDamping()
    params['external'] = external

    sys = System(params)
    sys.name = "OddPolyLin"

    return sys

def ExternalTar(num_weights=ODD_POLY_WEIGHTS, external=basis.Null()):

    params = dict()

    params['feedback1'] = basis.Null()
    params['feedback2'] = basis.Null()
    params['stiffness'] = basis.OddPoly(num_weights=1)
    params['damping'] = basis.LinearDamping(num_weights=1)
    params['external'] = external

    sys = System(params)
    sys.name = "External"

    return sys

def TanSigLeonard(num_stiffness=TAN_SIG_WEIGHTS, num_damping=LEONARD_WEIGHTS):

    sys = System()
    sys.feedback1 = basis.TanSig(num_weights=num_stiffness)
    sys.feedback2 = basis.Leonard(num_weights=num_damping)
    sys.name = "TanSigLeonard"

    return sys

def OddPolyLeonard(num_stiffness=ODD_POLY_WEIGHTS, num_damping=LEONARD_WEIGHTS):

    params = dict()
    params['feedback1'] = basis.OddPoly(num_weights=num_stiffness)
    params['feedback2'] = basis.Leonard(num_weights=num_damping)
    params['name'] = "OddPolyLeonard"

    sys = System(params)

    return sys

def DMP(num_weights=DMP_WEIGHTS):

    sys = System()
    sys.feedback1 = basis.OddPoly(num_weights=DMP_WEIGHTS)
    centres = [-7.5, -2.5, -1, 0, 1, 2.5, 7.5]
    variances = [1.0/3, 1.0/5, 1.0/5, 1.0/5, 1.0/5, 1.0/5, 1.0/3]
    sys.feedback2 = basis.DMP(centres, variances)
    sys.name = "DMP"

    return sys

def Stiffness(num_weights=4):

    params = dict()
    params['feedback1'] = basis.Stiffness(num_weights=num_weights)

    sys = System(params)
    sys.name = "General stiffness, Linear Damping"

    return sys

def StiffLeonard(num_weights=4):

    params = dict()
    params['feedback1'] = basis.Stiffness(num_weights=num_weights)
    params['feedback2'] = basis.Leonard(num_weights=2)

    sys = System(params)
    sys.name = "General stiffness, Leonard Damping"

    return sys

def StiffLin(num_weights=4):

    params = dict()
    params['feedback1'] = basis.Stiffness(num_weights=num_weights)
    params['feedback2'] = basis.LinearDamping(num_weights=1)

    sys = System(params)
    sys.name = "General stiffness, Linear Damping"

    return sys

def StiffDamp(num_weights=4):

    params = dict()
    params['feedback1'] = basis.Stiffness(num_weights=num_weights)
    params['feedback2'] = basis.Damping(num_weights=num_weights)

    sys = System(params)
    sys.name = "General stiffness, Linear Damping"

    return sys

def StiffLinObs(num_weights=4):

    params = dict()
    params['feedback1'] = basis.Stiffness(num_weights=num_weights)
    params['feedback2'] = basis.CubicDamping(num_weights=2)

    obs = msd_environment.Obstacle([0, 3], [-1., -0.01], 1)
    params['obstacles'] = [obs]

    sys = System(params)
    sys.name = "General stiffness, Linear Damping"

    return sys






