#!/usr/bin/env python
'''A variety of output functions to use'''
def all_output():
	return (lambda x: x, 'all')

def pos_output():
	return (lambda x: [x[0]], 'pos')

def vel_output():
	return (lambda x: [x[1]], 'vel')

def ke_output():
	return (lambda x: [0.5 * x[1]*x[1]], 'KE')

