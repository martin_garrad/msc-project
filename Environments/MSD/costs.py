import numpy as np
import wx

'''This class represents a generalised cost function for the MSD System
The total cost is a weighted sum of four components:
-Terminal state
-Collisions with obstacles
-Control effort
-Trajectory Following
'''

class Cost:

    def __init__(self, weights, dt, traj_length):
        self.terminal = self.mse
        self.obstacle = self.potential_cost
        self.control = self.quadratic_control_cost
        self.output = self.mse
        self.weights = weights
        self.dt = dt
        self.traj_length = traj_length
        self.trajectory_weights = [1.0 for _ in range(int(self.traj_length/self.dt)+1)]
        self.potential_threshold = 0.1

    def update_time(self, dt, traj_length):
        self.dt = dt
        self.traj_length = traj_length

    def get_cost(self, trajectory, reference, obstacles=None, actions=None):

        self.terminal_cost = self.terminal([trajectory[-1]], [reference[-1]])

        if obstacles:
            self.obstacle_cost = self.obstacle(trajectory, obstacles)
        else:
            self.obstacle_cost = 0.0, [0.0 for _ in range(int(self.traj_length/self.dt)+1)]

        if actions:
            self.control_cost = self.control(actions)
        else:
            self.control_cost = 0.0, [0.0 for _ in range(int(self.traj_length/self.dt)+1)]


        self.output_cost = self.output(trajectory, reference)

        self.cost_list = [(self.weights[1] * c1) + (self.weights[3]*c2) for (c1, c2) in zip(self.obstacle_cost[1], self.output_cost[1])]
        self.total_cost = (self.weights[0] * self.terminal_cost[0]) + (self.weights[1] * self.obstacle_cost[0]) + (self.weights[2] * self.control_cost[0]) + (self.weights[3] * self.output_cost[0])

        self.cost_sum = np.cumsum(self.cost_list[::-1])
        self.future_costs = self.cost_sum[::-1]

        return self.total_cost, self.cost_list

    '''
    def get_cost(self, trajectory, reference, obstacles=None, actions=None):

        #This function was hacked on at the end of the project. 
        #It is quick and dirty - if extended, rewrite to conform to other cost functions.
        #Or just re-write the whole cost architecture, which is a mess frankly. 

        self.cost_list = list()
        zero_crossed = False
        for i, p in enumerate(trajectory):
            if p < 0:
                zero_crossed = True

            if zero_crossed:
                if abs(p) > 0.1:
                    self.cost_list.append(p**2)
                else:
                    self.cost_list.append(0.)
            else:
                self.cost_list.append(10.*p**2)

        self.total_cost = np.sum(self.cost_list)

        self.cost_sum = np.cumsum(self.cost_list[::-1])
        self.future_costs = self.cost_sum[::-1]

        return self.total_cost, self.cost_list
    '''

    def get_timestep_cost(self, state, reference, obstacles, action):

        obstacle_cost = self.obstacle([state], obstacles)
        control_cost = self.control_cost([action])
        output_cost = self.output_cost([state], [reference])

    def obstacle_cost(self, trajectory, obstacles):

        #Calculates the cost of entering any regions with obstacles

        seq = [np.sum([obstacle.weight * (i in range(int(obstacle.timesteps[0]/self.dt), int(obstacle.timesteps[0]/self.dt) + int((obstacle.timesteps[1]-obstacle.timesteps[0])/self.dt))) * (t>obstacle.region[0])*(t<obstacle.region[1]) for obstacle in obstacles]) for (i, t) in enumerate(trajectory)]

        return np.sum(seq), seq

    def potential_cost(self, trajectory, obstacles):

        seqs = []
        for (i, t) in enumerate(trajectory):
            seq = []
            for obstacle in obstacles:
                if i in range(int(obstacle.timesteps[0]/self.dt), int(obstacle.timesteps[0]/self.dt) + int((obstacle.timesteps[1]-obstacle.timesteps[0])/self.dt)):

                    midpoint = np.mean([obstacle.region[0], obstacle.region[1]])
                    dist = np.sqrt((t-midpoint)**2)
                    cost = np.exp(-dist)

                    if dist < abs(obstacle.region[1] - obstacle.region[0])/2.:
                        seq.append(cost)
                    else:
                        seq.append(0.)
                else:
                    seq.append(0.)
            seqs.append(np.sum(seq))

        print "dist is %f" % dist
        print "midpoint is %f" % midpoint
        print "Obstacle cost is %f" % np.sum(seqs)

        return np.sum(seqs), seqs

    def quadratic_control_cost(self, actions):

        #Calculates a simple quadratic control cost

        seq = [a**2 for a in actions]

        return np.sum(seq), seq

    def mse(self, target_trajectory, reference_trajectory, state_weights=None):

        if not state_weights:
            state_weights = [1.0 for s in range(len(target_trajectory[0]))]

        seq = [t_weights * np.sum([state_w * ((t-r)**2) for (t, r, state_w) in zip(t_state, r_state, state_weights)]) for (t_state, r_state, t_weights) in zip(target_trajectory, reference_trajectory, self.trajectory_weights)]

        return np.sum(seq), seq

    def mae(self, target_trajectory, reference_trajectory, state_weights=None):
        if not state_weights:
            state_weights = [1.0 for s in range(len(target_trajectory[0]))]

        seq = [t_weights * np.sum([state_w * (abs(t-r)) for (t, r, state_w) in zip(t_state, r_state, state_weights)]) for (t_state, r_state, t_weights) in zip(target_trajectory, reference_trajectory, self.trajectory_weights)]

        return np.sum(seq), seq

    def plot_traj_weights(self, panel=None):

        if not panel:
            fig = plt.figure()
        else:
            fig = panel.fig

        ax = fig.add_subplot(1, 1, 1)
        ax.plot(self.trajectory_weights)

        if panel:
            panel.canvas.draw()
        else:
            fig.show()

class CostPanel(wx.Panel):

    def __init__(self, parent, cost):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.cost = cost
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Cost Function", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.terminal_weight_label = wx.StaticText(self, -1, label="Terminal Weight")
        self.obstacles_weight_label = wx.StaticText(self, -1, label="Obstacle Weight")
        self.control_weight_label = wx.StaticText(self, -1, label="Control Weight")
        self.output_weight_label = wx.StaticText(self, -1, label="Output Weight")

        self.terminal_weight = wx.TextCtrl(self, -1)
        self.obstacles_weight = wx.TextCtrl(self, -1)
        self.control_weight = wx.TextCtrl(self, -1)
        self.output_weight = wx.TextCtrl(self, -1)

        self.terminal_weight.SetValue(str(self.cost.weights[0]))
        self.obstacles_weight.SetValue(str(self.cost.weights[1]))
        self.control_weight.SetValue(str(self.cost.weights[2]))
        self.output_weight.SetValue(str(self.cost.weights[3]))

        self.start_label = wx.StaticText(self, -1, label="Start time")
        self.end_label = wx.StaticText(self, -1, label="End Time")
        self.traj_weight_label = wx.StaticText(self, -1, label="New Weighting")


        self.start = wx.TextCtrl(self, -1)
        self.end = wx.TextCtrl(self, -1)
        self.traj_weight = wx.TextCtrl(self, -1)

        self.update_weights_button = wx.Button(self, -1, label="Update weights")
        self.update_traj_weights_button = wx.Button(self, -1, label="Update trajectory weighting")
        self.plot_traj_weights_button = wx.Button(self, -1, label="Plot trajectory weights")

        self.Bind(wx.EVT_BUTTON, self.update_weights, self.update_weights_button)
        self.Bind(wx.EVT_BUTTON, self.update_traj_weights, self.update_traj_weights_button)
        self.Bind(wx.EVT_BUTTON, self.plot_traj_weights, self.plot_traj_weights_button)

        self.sizer.Add(self.title, (0, 0), flag=wx.EXPAND)
        self.sizer.Add(self.terminal_weight_label, (1, 0), flag=wx.EXPAND)
        self.sizer.Add(self.obstacles_weight_label, (2, 0), flag=wx.EXPAND)
        self.sizer.Add(self.control_weight_label, (3, 0), flag=wx.EXPAND)
        self.sizer.Add(self.output_weight_label, (4, 0), flag=wx.EXPAND)

        self.sizer.Add(self.terminal_weight, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.obstacles_weight, (2, 1), flag=wx.EXPAND)
        self.sizer.Add(self.control_weight, (3, 1), flag=wx.EXPAND)
        self.sizer.Add(self.output_weight, (4, 1), flag=wx.EXPAND)

        self.sizer.Add(self.update_weights_button, (5, 0), flag=wx.EXPAND)

        self.sizer.Add(self.start_label, (1, 2), flag=wx.EXPAND)
        self.sizer.Add(self.end_label, (2, 2), flag=wx.EXPAND)
        self.sizer.Add(self.traj_weight_label, (3, 2), flag=wx.EXPAND)

        self.sizer.Add(self.start, (1, 3), flag=wx.EXPAND)
        self.sizer.Add(self.end, (2, 3), flag=wx.EXPAND)
        self.sizer.Add(self.traj_weight, (3, 3), flag=wx.EXPAND)

        self.sizer.Add(self.update_traj_weights_button, (4, 2), flag=wx.EXPAND)
        self.sizer.Add(self.plot_traj_weights_button, (5, 2), flag=wx.EXPAND)

        self.SetSizerAndFit(self.sizer)
        self.Layout()

    def plot_traj_weights(self, event):

        self.cost.plot_traj_weights(self.parent.parent.EnvPlot)

    def update_traj_weights(self, event):

        start = float(self.start.GetValue())
        end = float(self.end.GetValue())
        weight = float(self.traj_weight.GetValue())

        start_index = int(start / self.cost.dt)
        end_index = int(end / self.cost.dt)
        self.cost.trajectory_weights[start_index:end_index] = [weight for _ in range(end_index-start_index)]

    def update_weights(self, event):

        self.cost.weights[0] = float(self.terminal_weight.GetValue())
        self.cost.weights[1] = float(self.obstacles_weight.GetValue())
        self.cost.weights[2] = float(self.control_weight.GetValue())
        self.cost.weights[3] = float(self.output_weight.GetValue())






