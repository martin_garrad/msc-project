#!/usr/bin/env python
import numpy as np
import matplotlib as mpl 
import matplotlib.pyplot as plt 
import seaborn as sn 
from scipy.integrate import odeint
import basis
import output
import msd
import random
import costs
import time

'''A simple mass-spring-damper simulation'''
MASS = 1.0
STIFFNESS = lambda x: 1.0*x
DAMPING = lambda x: 0.0
EXTERNAL = lambda t: 0.0
INIT_POS = 2.0
INIT_VEL = 0.0
DT = 0.001

class System:

    def __init__(self, params=dict()):

        #Create default values - these will be overwritten if specified in params
        self.mass1 = MASS
        self.mass2 = MASS
        self.stiffness1 = STIFFNESS
        self.damping1 = DAMPING
        self.stiffness2 = lambda x: 3.0 * x
        self.damping2 = DAMPING
        self.external = EXTERNAL
        self.feedback1 = basis.Null()
        self.feedback2 = basis.Null()
        self.feedback3 = basis.TanSig(num_weights=2)
        self.feedback4 = basis.Leonard()
        self.output = lambda x: x[0] + x[2]
        self.init_pos1 = INIT_POS
        self.init_vel1 = INIT_VEL
        self.init_pos2 = INIT_POS
        self.init_vel2 = INIT_VEL
        self.dt = DT

        #Overwrite defaults 
        for (k, v) in params.iteritems():
            setattr(self, k, v)

        #Initalise storage
        self.next_pos = (self.init_pos1, self.init_pos2)
        self.next_vel = (self.init_vel1, self.init_vel2)
        self.history = []
        self.output_history = []
        self.time_history = []

    def reset(self):
        #Reset to initial state

        self.next_pos = (self.init_pos1, self.init_pos2)
        self.next_vel = (self.init_vel1, self.init_vel2)

    def solve(self, t):
        #Solve for a set period of time

        #Get initial values
        z0 = np.array([self.next_pos[0], self.next_vel[0], self.next_pos[1], self.next_vel[1]])

        #Create state space gradient vector
        def grad(z, t):

            return np.array(
            [z[1],
            ((-self.feedback1.func(z[0])-self.stiffness1(z[0])) + (-self.feedback2.func([z[0], z[1]]) - self.damping1([z[0], z[1]])) + (self.feedback3.func(z[2])+self.stiffness2(z[2])) + (self.feedback4.func([z[2], z[3]]) + self.damping2([z[2], z[3]])))/self.mass1,
            z[3],
            ((-self.feedback3.func(z[2])-self.stiffness2(z[2])) + (-self.feedback4.func([z[2], z[3]]) - self.damping2([z[2], z[3]])))/self.mass2]
            )
            
        #Store time series            
        self.times = np.linspace(0.0, t, num=np.round(t/self.dt)+1)

        #Solve system and calculate output / reference signal
        self.state = odeint(grad, z0, self.times)

        #Calculate output
        self.outputs = np.array(map(self.output, self.state))

        if self.history == []:
            self.history = self.state
            self.output_history = self.outputs
            self.time_history = self.times
        else:
            np.concatenate((self.history, self.state))
            np.concatenate((self.output_history, self.outputs))
            np.concatenate((self.time_history, self.times))

        return self.outputs

    def plot_pos(self, axes=None, show=False, offset=0, history=False, color='-b'):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.history[offset:, 2], color)
        else:
            axes.plot(self.times[offset:], self.state[offset:, 2], color)
        axes.set_ylabel('Position', fontsize=8)
        axes.set_xlabel('Time(s)', fontsize=8)
        axes.tick_params(axis='both', which='major', labelsize=10)

        if show:
            fig.show()

    def plot_vel(self, axes=None, show=False, offset=0, history=False,color='-b'):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.history[offset:, 3], color)
        else:
            axes.plot(self.times[offset:], self.state[offset:, 3], color)
        axes.set_ylabel('Velocity', fontsize=8)
        axes.set_xlabel('Time(s)', fontsize=8)
        axes.tick_params(axis='both', which='major', labelsize=10)

        if show:
            fig.show()

    def plot_state(self, axes=None, show=False, offset=0, history=False, color='-b'):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.history[offset:, 2], self.history[offset:, 3], color)
        else:
            axes.plot(self.state[offset:, 2], self.state[offset:, 3], color)
        axes.set_ylabel('Velocity', fontsize=8)
        axes.set_xlabel('Position', fontsize=8)
        axes.tick_params(axis='both', which='major', labelsize=10)

        if show:
            fig.show()

    def plot_output(self, axes=None, show=False, offset=0, history=False, color='-b'):

        if not hasattr(self, 'state'):
            print "System must be solved before plotting"
            return

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show = True

        if history:
            axes.plot(self.time_history[offset:], self.output_history[offset:], color)
        else:
            axes.plot(self.times[offset:], self.outputs[offset:], color)
        axes.set_ylabel('System Output', fontsize=8)
        axes.set_xlabel('Time(s)', fontsize=8)
        axes.tick_params(axis='both', which='major', labelsize=10)

        if show:
            fig.show()

class Environment:

    def __init__(self):

        self.target = System()
        self.reference = msd.System()
        '''
        self.reference.stiffness = lambda x: 20*x
        self.reference.damping = lambda x: -20.0 * (2-x[0]**2)*x[1]
        '''
        self.reference.stiffness = lambda x: 10*x + 2.5*x**3
        self.reference.damping = lambda x: 1.5*x[1]
        
        self.reference.output = output.pos_output()
        self.cost = costs.mse
        self.history = []

    def episode(self, length=5.0, init_pos = 1.0, init_vel = 1.0):

        self.target.init_pos = (init_pos, init_pos)
        self.target.init_vel = (init_vel, init_vel)

        self.reference.init_pos = init_pos
        self.reference.init_vel = init_vel

        self.target.reset()
        self.reference.reset()

        self.target_output = self.target.solve(length)
        self.reference_output = self.reference.solve(length)

        #self.reward = self.cost(self.target_output[:, 0], self.reference_output[:, 0], self.target_output[:, 1], self.reference_output[:, 1], 1.0, 5.0)
        self.reward = self.cost(self.target_output, self.reference_output)

        return self.reward

    def plot(self, fig=None, show=False, history=False):

        if not fig:
            fig = plt.figure()
            show = True

        axes = fig.add_subplot(2, 3, 1)
        self.target.plot_pos(axes, history=history)
        axes = fig.add_subplot(2, 3, 2)
        self.target.plot_vel(axes, history=history)
        axes = fig.add_subplot(2, 3, 3)
        self.target.plot_state(axes, history=history)

        axes = fig.add_subplot(2, 3, 4)
        self.reference.plot_pos(axes, history=history, color='-r')
        axes = fig.add_subplot(2, 3, 5)
        self.reference.plot_vel(axes, history=history, color='-r')
        axes = fig.add_subplot(2, 3, 6)
        self.reference.plot_state(axes, history=history, color='-r')


        if show:
            fig.show()

    def plot_outputs(self, fig=None, show=False, history=False):

        if not fig:
            fig = plt.figure()
            show = True

        axes = fig.add_subplot(1, 2, 1)
        self.target.plot_output(axes, history=history)
        axes = fig.add_subplot(1, 2, 2)
        self.reference.plot_output(axes, history=history, color='-r')

    def plot_cost(self, fig=None, show=False):

        if not fig:
            fig = plt.figure()
            show=True

        axes = fig.add_subplot(1, 1, 1)
        cost = ([abs(t-r) for (t, r) in zip(self.target_output, self.reference_output)])
        axes.plot(cost, 'g')

        if show:
            fig.show()

def stochastic_hillclimber():

    plt.ion()

    w1 = 100 * random.random()
    w2 = 100 * random.random()
    w3 = 5 * random.random()
    w4 = 50 * random.random()

    width1 = 10.0 * random.random()
    width2 = 10.0 * random.random()

    initX = 2.0
    initV = 0.0

    Env = Environment()

    steps = 1000
    step = 0

    Env.target.feedback3.update([w1, w2], [width1, width2])
    Env.target.feedback4.update([w3, w4])

    best_cost = Env.episode(20.0, initX, initV)

    fig1 = plt.figure()
    Env.plot(fig1)
    fig1.show()

    while step < steps and best_cost > 0.1:

        step+=1
        step_size = 10.0 * np.exp(-1.0*(float(step)/float(steps)))

        if step%6==0:
            new_w1 = max(0.0, w1 - (step_size) + (2.0 * step_size * random.random()))
            new_w2 = w2
            new_w3 = w3
            new_w4 = w4
            new_width1 = width1
            new_width2 = width2

        elif step%6==1:
            new_w1 = w1
            new_w2 = max(0.0, w2 - (step_size) + (2.0 * step_size * random.random()))
            new_w3 = w3
            new_w4 = w4
            new_width1 = width1
            new_width2 = width2

        elif step%6==2:
            new_w1 = w1
            new_w2 = w2
            new_w3 = w3
            new_w4 = w4
            new_width1 = max(1.0, width1 - step_size + (2.0 * step_size * random.random()))
            new_width2 = width2
        elif step%6==3:
            new_w1 = w1
            new_w2 = w2
            new_w3 = w3
            new_w4 = w4
            new_width1 = width1
            new_width2 = max(1.0, width2 - step_size + (2.0 * step_size * random.random()))

        elif step%6==4:
            new_w1 = w1
            new_w2 = w2
            new_width1 = width1
            new_width2 = width2
            new_w3 = min(0.0, w3 - (step_size/4) + (.5 * step_size * random.random()))
            new_w4 = w4

        elif step%6==5:
            new_w1 = w1
            new_w2 = w2
            new_width1 = width1
            new_width2 = width2
            new_w3 = w3
            new_w4 = w4 - (step_size/4) + (.5 * step_size * random.random())

        Env.target.feedback3.update([new_w1, new_w2], [new_width1, new_width2])
        Env.target.feedback4.update([new_w3, new_w4])

        print new_w1, new_w2, new_w3, new_w4
        cost = Env.episode(20.0, initX, initV)

        fig1.clear()
        Env.plot(fig1)
        plt.draw()
        time.sleep(.0001)

        if cost < best_cost:
            w1 = new_w1
            w2 = new_w2
            w3 = new_w3
            w4 = new_w4
            width1 = new_width1
            width2 = new_width2
            best_cost = cost

            print "Weigth 1: %f, Weigth 2: %f" % (w1, w2) 
            print "Width 1: %f, Width 2: %f" % (width1, width2)
            print "Damping weight 1: %f, weight 2: %f" % (w3, w4)
            print "On step %d I found cost %f with step size %f" % (step, best_cost, step_size)
            print

    fig2 = plt.figure()
    fig3 = plt.figure()
    Env.plot(fig2)
    Env.plot_cost(fig3)
    fig2.show()
    fig3.show()
    raw_input()



if __name__=="__main__":
    stochastic_hillclimber()




