import numpy as np
import math
import matplotlib.pyplot as plt
plt.ion()
from matplotlib.patches import Rectangle
import seaborn as sn
import agent
import copy
import gui
import wx
import time
from math import pi as pi
import random

class Rollout:
    def __init__(self, environment):

        self.total_cost = environment.reward
        self.costs = environment.reward_list
        cost_sum = np.cumsum(self.costs[::-1])
        self.future_costs = cost_sum[::-1]
        self.states = environment.history
        #Actions is a list of the actual forces applied, while means are the centres of the gaussians from which the actions are drawn
        self.actions = environment.actions
        self.means = environment.means
        self.variances = environment.variances
        self.activations = environment.activations

'''Simple implementation of the cartpole problem to test implemented RL algorithms'''

class Cartpole:
    def __init__(self, cost, fig=None):

        self.dt = 1. / 60

        self.cart_mass = 1.0
        self.pole_mass = 0.15
        self.pole_length = 0.75
        self.cart_size = (0.2, 0.2)
        self.noise = 0.1
        self.force_range = [-1000.0, 1000.0]

        self.init_pos = 0
        self.init_vel = 0
        self.init_theta = 0.1
        self.init_theta_dot = 0.0

        self.gravity = -9.81

        self.state = np.array([self.init_pos, self.init_vel, self.init_theta, self.init_theta_dot])
        self.reward_list = []
        self.history = []
        self.actions = []
        self.means = []
        self.activations = []
        self.variances = []
        self.rollouts = []

        #Analytic solution from NAC Paper: K = [5.71, 11.3, -82.1, -21.6]
        self.weights = [-3.71, -10.3, -80.1, -22.6]
        self.num_weights = len(self.weights)
        self.target_weights = []

        self.add_noise = self.white_noise
        self.cost = cost

    def perturb(self, steps):

        self.weights = [w + random.random()*steps[i] for (i, w) in enumerate(self.weights)]

    def reset(self):

        self.init_pos = 0
        self.init_vel = 0
        self.init_theta = 0.1
        self.init_theta_dot = 0.0

        self.state = np.array([self.init_pos, self.init_vel, self.init_theta, self.init_theta_dot])

        self.reward_list = []
        self.history = []
        self.actions = []
        self.means = []
        self.activations = []
        self.variances = []

    def clear_rollouts(self):

        self.rollouts = []

    def grad(self, state, action):

        dpos = state[1]
        dtheta = state[3]

        dvel = (action + self.pole_mass * math.sin(self.state[2])*(self.pole_length*(self.state[3]**2)+ (self.gravity*math.cos(self.state[2])))) / (self.cart_mass + (self.pole_mass*(math.sin(self.state[2])**2))) 
        dtheta_dot = (-action*math.cos(self.state[2]) - (self.pole_mass*self.pole_length*(self.state[2]**2)*math.cos(self.state[2])*math.sin(self.state[2])) - ((self.cart_mass+self.pole_mass)*self.gravity*math.sin(self.state[2]))) / (self.pole_length * (self.cart_mass + (self.pole_mass * (math.sin(self.state[2])**2))))
        #This is noise in the state-transition
        noise = self.noise *  np.diag([0.5, 2, 2, 2])
        
        return np.array([dpos+noise[0, 0], dvel+noise[1, 1], dtheta+noise[2, 2], dtheta_dot+noise[3, 3]])


    def plot_cart(self, panel=None):

        if panel == None:
            fig = plt.figure()
            ax1 = fig.add_subplot(1, 2, 1)
            ax2 = fig.add_subplot(1, 2, 2)
        else:
            fig = panel.fig
            fig.clear()
            ax1 = fig.add_subplot(1, 2, 1)
            ax2 = fig.add_subplot(1, 2, 2)

        ax2.plot(self.actions)

        if self.history:
            for state in self.history:

                ax1.add_patch(Rectangle((state[0] - self.cart_size[0]/2.0, 0), self.cart_size[0], self.cart_size[1], facecolor="gray"))

                pole_x = (state[0], state[0] + self.pole_length * math.sin(state[2]))
                pole_y = (self.cart_size[1], self.cart_size[1] + self.pole_length * math.cos(state[2]))

                ax1.plot(pole_x, pole_y, '-r')

                ax1.set_xlim((-5, 5))
                ax1.set_ylim((0, 10))


                if panel == None:
                    plt.draw()
                else:
                    panel.canvas.draw()
                    time.sleep(0.0001)
                    ax1.clear()

    def plot(self, fig=None):

        if fig == None:
            fig = plt.figure()

        fig.clear()

        ax = fig.add_subplot(2, 3, 1)
        ax.plot(np.array(self.history)[:, 0])
        ax.set_xlabel('Time', fontsize=12)
        ax.set_ylabel('Cart position', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax = fig.add_subplot(2, 3, 2)
        ax.plot(np.array(self.history)[:, 1])
        ax.set_xlabel('Time', fontsize=12)
        ax.set_ylabel('Cart velocity', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax = fig.add_subplot(2, 3, 4)
        ax.plot(np.array(self.history)[:, 2])
        ax.set_xlabel('Time', fontsize=12)
        ax.set_ylabel('Pole angle', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax = fig.add_subplot(2, 3, 5)
        ax.plot(np.array(self.history)[:, 3])
        ax.set_xlabel('Time', fontsize=12)
        ax.set_ylabel('Pole Velocity', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax = fig.add_subplot(2, 3, 3)
        self.plot_cost(ax)
        ax.set_xlabel('Time', fontsize=12)
        ax.set_ylabel('Cost', fontsize=12)
        ax.tick_params(axis='both', which='major', labelsize=12)

        fig.tight_layout()


    def get_action(self):
        #Add exploration noise
        action = self.add_noise()
        #Store policy action before applying control limits
        self.actions.append(action)
        #Apply control limits
        action = max(min(action, self.force_range[1]), self.force_range[0])


        return action

    def white_noise(self):

        activation = [x for x in self.state]
        mean = np.sum([-w*x for (x,w) in zip(self.state, self.weights)])

        variance = 1.
        self.variances.append(variance)
        self.activations.append(activation)
        self.means.append(mean)

        return np.random.normal(mean, variance)

    def param_noise(self):

        activation = [x for x in self.state]

        p_weights = [np.random.normal(w,v) for (w, v) in zip(self.weights, self.weight_var)]

        mean = np.sum([-w*x for (x, w) in zip(self.state, self.weights)])

        self.variances.append([p-w for (p, w) in zip(p_weights, self.weights)])
        self.activations.append(activation)
        self.means.append(mean)

        return mean

    def action_noise(self):

        activation = [x for x in self.state]
        mean = np.sum([-w*x for (x,w) in zip(self.state, self.weights)])
        action_noise = [np.random.normal(0, v) for v in sef.action_vars]

        self.variances.append(action_noise)
        self.activations.append(activation)
        self.means.append(mean)

        return mean+np.sum(action_noise)



    def solve(self, length):

        failure_time = int(length/self.dt)

        for t in range(int(length/self.dt)):

            action = self.get_action()
            self.state += self.dt * self.grad(self.state, action)

            self.history.append(copy.deepcopy(self.state))

            if abs(self.state[2]) > math.pi / 6. or abs(self.state[0]) > 100.5:
                failure_time = t 
                break

        failure_steps = (int(length/self.dt) - failure_time)
        cost, cost_seq = (self.cost.get_cost(self.history, self.actions, failure_steps))

        self.reward_list = cost_seq

        self.reward = cost

    def episode(self, length=10.0):

        self.reset()
        self.solve(length)
        self.rollouts.append(Rollout(self))

        return self.reward

    def update(self, weights):

        self.target_weights.append(copy.deepcopy(self.weights))
        self.weights = weights

    def plot_cost(self, axes=None, show=False):

        if not axes:
            fig = plt.figure()
            show= True
            axes = fig.add_subplot(1, 1, 1)

        axes.plot(self.reward_list, 'g')
        axes.plot(np.cumsum(self.reward_list), 'r')

        if show:
            fig.tight_layout()
            fig.show()

    def make_param_panel(self, parent):

        return CartPanel(parent)

class CartPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Enviroment Parameters", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.dt_label = wx.StaticText(self, -1, label="Timestep")
        self.cart_mass_label = wx.StaticText(self, -1, label="Cart mass")
        self.cart_size_label = wx.StaticText(self, -1, label="Cart size")
        self.pole_mass_label = wx.StaticText(self, -1, label="Pole mass")
        self.pole_length_label = wx.StaticText(self, -1, label="Pole length")
        self.gravity_label = wx.StaticText(self, -1, label="Gravity")
        self.init_conditions_label = wx.StaticText(self, -1, label="Initial conditions")
        self.weights_label = wx.StaticText(self, -1, label="Weights")

        self.dt = wx.TextCtrl(self, -1)
        self.dt.SetValue(str(self.parent.environment.dt))
        self.cart_mass = wx.TextCtrl(self, -1)
        self.cart_mass.SetValue(str(self.parent.environment.cart_mass))
        self.cart_size = wx.TextCtrl(self, -1,)
        self.cart_size.SetValue(str(self.parent.environment.cart_size))
        self.pole_mass = wx.TextCtrl(self, -1)
        self.pole_mass.SetValue(str(self.parent.environment.pole_mass))
        self.pole_length = wx.TextCtrl(self, -1)
        self.pole_length.SetValue(str(self.parent.environment.pole_length))
        self.gravity = wx.TextCtrl(self, -1)
        self.gravity.SetValue(str(self.parent.environment.gravity))
        self.init_conditions = wx.TextCtrl(self, -1)
        self.init_conditions.SetValue("[%.2f, %.2f, %.2f, %.2f]" % (self.parent.environment.init_pos, self.parent.environment.init_vel, self.parent.environment.init_theta, self.parent.environment.init_theta_dot))
        self.weights = wx.TextCtrl(self, -1)
        self.weights.SetValue(str(self.parent.environment.weights))

        self.update_button = wx.Button(self, -1, "Update Enviroment")
        self.animate_button = wx.Button(self, -1, "Animate Current Policy")
        self.plot_button = wx.Button(self, -1, "Plot Enviroment")

        self.Bind(wx.EVT_BUTTON, self.on_animate_button, self.animate_button)
        self.Bind(wx.EVT_BUTTON, self.on_update_button, self.update_button)
        self.Bind(wx.EVT_BUTTON, self.on_plot_button, self.plot_button)

        self.sizer.Add(self.title, (0, 0), flag=wx.ALL)
        self.sizer.Add(self.dt_label, (1, 0), flag=wx.EXPAND)
        self.sizer.Add(self.cart_mass_label, (2, 0), flag=wx.EXPAND)
        self.sizer.Add(self.cart_size_label, (3, 0), flag=wx.EXPAND)
        self.sizer.Add(self.pole_mass_label, (4, 0), flag=wx.EXPAND)
        self.sizer.Add(self.pole_length_label, (5, 0), flag=wx.EXPAND)
        self.sizer.Add(self.gravity_label, (6, 0), flag=wx.EXPAND)
        self.sizer.Add(self.init_conditions_label, (7, 0), flag=wx.EXPAND)
        self.sizer.Add(self.weights_label, (8, 0), flag=wx.EXPAND)

        self.sizer.Add(self.dt, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.cart_mass, (2, 1), flag=wx.EXPAND)
        self.sizer.Add(self.cart_size, (3, 1), flag=wx.EXPAND)
        self.sizer.Add(self.pole_mass, (4, 1), flag=wx.EXPAND)
        self.sizer.Add(self.pole_length, (5, 1), flag=wx.EXPAND)
        self.sizer.Add(self.gravity, (6, 1), flag=wx.EXPAND)
        self.sizer.Add(self.init_conditions, (7, 1), flag=wx.EXPAND)
        self.sizer.Add(self.weights, (8, 1), flag=wx.EXPAND)

        self.sizer.Add(self.update_button, (10, 0), flag=wx.ALL)
        self.sizer.Add(self.animate_button, (11, 0), flag=wx.ALL)
        self.sizer.Add(self.plot_button, (12, 0), flag=wx.ALL)

        self.SetSizer(self.sizer)
        self.Layout()

    def on_animate_button(self, event):
        
        self.parent.environment.episode()
        self.parent.environment.plot_cart(self.parent.EnvPlot)

    def on_update_button(self, event):
        pass

    def on_plot_button(self, event):

        self.parent.environment.episode()
        self.parent.environment.plot(self.parent.EnvFig)
        self.parent.EnvCanvas.draw()








