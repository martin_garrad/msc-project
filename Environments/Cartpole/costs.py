import numpy as np

class Cost:

	def __init__(self, Q, R):
		self.Q = Q
		self.R = R

	def get_cost(self, trajectory, actions, failure_steps):

		output_cost_seq = [np.sum([q*(t_i**2) for (q, t_i) in zip(self.Q, t)]) for t in trajectory] + [2.0 for t in range(failure_steps)]
		control_cost_seq = [self.R*a**2 for a in actions]

		cost = np.sum(control_cost_seq) + np.sum(output_cost_seq)

		return cost, [o+c for (o, c) in zip(output_cost_seq, control_cost_seq)] 

	def update_time(self, dt, length):
		pass

