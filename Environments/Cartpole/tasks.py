import cartpole
import costs

def default(cost=costs.Cost([1.25, 1., 12., 0.25], 0.01)):

	return cartpole.Cartpole(cost)