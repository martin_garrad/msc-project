#!/usr/bin/env python
import numpy as np
import matplotlib as mpl 
import matplotlib.pyplot as plt 
import seaborn as sn 
from scipy.integrate import odeint
import basis
import output
import msd
import random
import costs
import time
import math

'''A simulation of a bar controlled by an attached spring'''

MASS = 1.0
STIFFNESS = lambda x: 1.0*x
DAMPING = lambda x: 0.01*x[1]
EXTERNAL = lambda t: 0.0
LENGTH = 2.0
INIT_THETA = 0.1
INIT_VEL = 0.0
DT = 0.1
Spring_Resting = 1.5

class System:

    def __init__(self, params=dict()):

        #Create default values - these will be overwritten if specified in params
        self.mass = MASS
        self.stiffness = STIFFNESS
        self.damping = DAMPING
        self.external = EXTERNAL
        self.feedback1 = basis.TanSig(num_weights=2)
        self.feedback2 = basis.Leonard()
        self.length = LENGTH
        self.output = lambda x: [self.length * math.cos(x[0]), self.length*math.sin(x[0])]
        self.init_theta = INIT_THETA
        self.init_vel = INIT_VEL
        self.dt = DT

        #Overwrite defaults 
        for (k, v) in params.iteritems():
            setattr(self, k, v)

        #Initalise storage
        self.next_pos = self.init_theta
        self.next_vel = self.init_vel
        self.history = []
        self.output_history = []
        self.time_history = []

    def reset(self):
        #Reset to initial state

        self.next_pos = self.init_pos
        self.next_vel = self.init_vel

    def solve(self, t):
        #Solve for a set period of time

        #Get initial values
        z0 = np.array([self.next_pos, self.next_vel])

        #Create state space gradient vector
        def grad(z, t):

            Rod_x = self.length * math.cos(z[0])
            Rod_y = self.length * math.sin(z[0])

            Spring_x = Rod_x
            Spring_y = Rod_y - (self.length/2.0)

            Spring_len = math.sqrt(Spring_x**2 + Spring_y**2)
            Spring_vel = (z[1] * self.length * (self.length/2.0) * math.cos(z[0])) / Spring_len
            Spring_ang = math.atan2(Spring_y, Spring_x)

            Total_force = -self.stiffness(Spring_len-Spring_Resting) - self.feedback1.func(Spring_len-Spring_Resting) - self.damping([Spring_len-Spring_Resting, Spring_vel]) - (self.feedback2.func([Spring_len-Spring_Resting, Spring_vel]))
            Applied_force = Total_force * math.cos(math.pi / 2 + Spring_ang - z[0])

            print "State is (%f, %f)" % (z[0], z[1])

            if z[0] < 0 and z[1] < 0 :
                return np.array([0.01, 0.0])
            elif z[0] > (math.pi / 2) and z[1] > 0:
                return np.array([-0.01, 0.0])
            
            print "Angle is :%f" % z[0]
            print "Speed is %f" % z[1]
            print "Spring len is %f" % Spring_len
            print "Force is %f\n" % Applied_force

            return np.array(
                [z[1],
             (-self.length * Applied_force)/self.mass]
             )

        #Store time series            
        self.times = np.linspace(0.0, t, num=np.round(t/self.dt)+1)

        #Solve system and calculate output / reference signal
        self.state = odeint(grad, z0, self.times)

        #Calculate output
        self.outputs = np.array(map(self.output, self.state))

        if self.history == []:
            self.history = self.state
            self.output_history = self.outputs
            self.time_history = self.times
        else:
            np.concatenate((self.history, self.state))
            np.concatenate((self.output_history, self.outputs))
            np.concatenate((self.time_history, self.times))

        return self.outputs

def main():
    plt.ion()

    sys = System()
    output = sys.solve(20)
    fig = plt.figure()
    fig.show()
    plt.autoscale(enable=False, axis='x')
    plt.autoscale(enable=False, axis='y')
    plt.xlim(-1, 3)
    plt.ylim(-1, 3)


    for i in range(int(20/sys.dt)):
        fig.clear()
        plt.plot([0, output[i, 0]], [0, output[i, 1]])
        plt.plot([0, output[i, 0]], [sys.length/2, output[i, 1]])
        plt.xlim(-1, 3)
        plt.ylim(-1, 3)
        plt.draw()
        #time.sleep(0.001)

if __name__=="__main__":
    main()
