#!/usr/bin/env python
import os
import pprint
import wx
import copy

import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar

import Environments
import agent
import matplotlib.pyplot as plt
import numpy as np

import types
import pkgutil
import yaml

plt.ion()
DPI = 50
FIG_SIZE = (24.0, 7.0)
PLOT_TIME = 10.0

AGENTS = agent.AGENTS
TASKS = []
COSTS = []

ENVIRONMENTS = [name for _, name, _ in pkgutil.iter_modules(['Environments'])]

params = {'backend': 'ps',
              'text.latex.preamble': ['\usepackage{gensymb}'],
              'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              'axes.titlesize': 8,
              'text.fontsize': 8, # was 10
              'legend.fontsize': 8, # was 10
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'text.usetex': True,
              'figure.figsize': [6.0,4.0],
              'font.family': 'serif'
    }

#matplotlib.rcParams.update(params)

class MainWindow(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(1250, 750))

        self.create_menu()

        self.sizer = wx.GridBagSizer()

        self.EnvPanel = EnvPanel(self)
        self.AgentPanel = AgentPanel(self)
        
        self.EnvPlot = PlotPanel(self)
        self.EnvParamPanel = ParamPanel(self)
        self.AgentPlot = PlotPanel(self)
        self.EnvFig = self.EnvPlot.fig
        self.LearnFig = self.AgentPlot.fig
        self.EnvCanvas = self.EnvPlot.canvas
        self.LearnCanvas = self.AgentPlot.canvas

        self.console = Console(self)

        self.sizer.Add(self.EnvPanel, (0, 0), flag=wx.GROW)
        self.sizer.Add(self.EnvParamPanel, (1, 0), flag=wx.GROW)
        self.plot_sizer = wx.BoxSizer(wx.VERTICAL)
        self.plot_sizer.Add(self.EnvPlot, 1, flag=wx.EXPAND)
        self.plot_sizer.Add(self.AgentPlot,1, flag=wx.EXPAND)
        self.sizer.Add(self.plot_sizer, (1, 1), flag=wx.EXPAND)
        self.sizer.Add(self.AgentPanel, (2, 0), span=(1, 1), flag=wx.GROW)
        self.sizer.Add(self.console, (2, 1), span=(1, 1), flag=wx.EXPAND)

        self.SetMinSize((900, 600))
        self.SetAutoLayout(True)
        self.SetSizer(self.sizer)
        self.Layout()
        self.Show()

    def make_param_panel(self):

        self.EnvParamPanel.Destroy()

        if self.EnvPanel.environment:
            self.EnvParamPanel = self.EnvPanel.environment.make_param_panel(self)
        else:
            self.EnvParamPanel = ParamPanel(self)
        self.sizer.Add(self.EnvParamPanel, (1, 0), flag=wx.GROW)
        self.Layout()

    def create_menu(self):

        self.menubar = wx.MenuBar()

        menu_file = wx.Menu()
        m_save_env = menu_file.Append(-1, "&Save environment", "Save environment")
        self.Bind(wx.EVT_MENU, self.on_save_env, m_save_env)
        m_save_agent = menu_file.Append(-1, "&Save Agent", "Save Agent")
        self.Bind(wx.EVT_MENU, self.on_save_agent, m_save_agent)
        m_save_all = menu_file.Append(-1, "&Save All", "Save All")
        self.Bind(wx.EVT_MENU, self.on_save_all, m_save_all)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
                
        self.menubar.Append(menu_file, "&File")
        self.SetMenuBar(self.menubar)

    def on_save_env(self, e):
        pass

    def on_save_agent(self, e):
        pass

    def on_save_all(self, e):
        pass

    def on_exit(self, e):
        self.Destroy()

    def write(self, string):
        print string

class PlotPanel(wx.lib.scrolledpanel.ScrolledPanel):
    def __init__(self, parent):
        self.dpi = DPI
        self.fig = Figure(FIG_SIZE, dpi=self.dpi)
        wx.lib.scrolledpanel.ScrolledPanel.__init__(self, parent, style=wx.SIMPLE_BORDER ^ wx.RESIZE_BORDER)
        self.SetupScrolling()
        self.SetMinSize((575, 250))

        self.canvas = FigCanvas(self, -1, self.fig)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Realize()
        self.fig.canvas.mpl_connect('button_press_event', self.on_subplot_click)

        self.Bind(wx.EVT_SIZE, self.on_size)
        self.zoom_state = 0

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.toolbar, 1, wx.EXPAND)
        self.sizer.Add(self.canvas, 0, wx.EXPAND)

        self.SetSizer(self.sizer)
        self.Fit()

    def on_size(self, event):
        pass

    def on_subplot_click(self, event):
        ax = event.inaxes
        if ax is None:
            # Occurs when a region not in an axis is clicked...
            return
        if event.button is 1:
            if self.zoom_state == 0:
            # On left click, zoom the selected axes
                ax._orig_position = ax.get_position()
                ax.set_position([0.1, 0.15, 0.85, 0.75])
                for axis in event.canvas.figure.axes:
                    # Hide all the other axes...
                    if axis is not ax:
                        axis.set_visible(False)
                self.zoom_state = 1
        elif event.button is 3:
            if self.zoom_state == 1:
            # On right click, restore the axes
                try:
                    ax.set_position(ax._orig_position)
                    for axis in event.canvas.figure.axes:
                        axis.set_visible(True)
                    self.zoom_state = 0
                except AttributeError:
                    # If we haven't zoomed, ignore...
                    pass
        else:
            # No need to re-draw the canvas if it's not a left or right click
            return
        event.canvas.draw()

class EnvPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Learning environment", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.sizer.Add(self.title, (0, 0), flag=wx.ALL)

        self.load_env_choice = wx.ComboBox(self, -1, choices=ENVIRONMENTS, name="Environment box")
        self.load_env_choice.IsEditable = False
        self.Bind(wx.EVT_COMBOBOX, self.on_env_change, self.load_env_choice)


        self.load_task_choice = wx.ComboBox(self, -1, choices=TASKS, name="Task box")
        self.load_task_choice.IsEditable = False

        self.load_task_button = wx.Button(self, -1, "Load New Environment")
        self.Bind(wx.EVT_BUTTON, self.on_load_task, self.load_task_button)

        self.sizer.Add(self.load_env_choice, (0, 1), flag=wx.EXPAND)
        self.sizer.Add(self.load_task_choice, (0, 2), flag=wx.EXPAND)
        self.sizer.Add(self.load_task_button, (0, 3), flag=wx.EXPAND)

        self.load_env_choice.SetSelection(0)
        self.on_env_change(None)
        
        self.SetSizer(self.sizer)
        self.Layout()

    def on_env_change(self, event):

        env = self.load_env_choice.GetValue()
        
        task_dir =  Environments.__dict__.get(env).__dict__.get('tasks')

        self.tasks = [a for a in dir(task_dir) if isinstance(task_dir.__dict__.get(a), types.FunctionType)]

        self.load_task_choice.SetItems(self.tasks)

        self.load_task_choice.SetSelection(0)
        
    def on_load_task(self, event):

        env = self.load_env_choice.GetValue()
        task = self.load_task_choice.GetValue()

        if env and task:
            self.environment = Environments.__dict__.get(env).__dict__.get('tasks').__dict__.get(task)()
            self.parent.environment = self.environment

            self.parent.make_param_panel()
        else:
            self.parent.write("No system loaded")

    def on_update_task(self, event):
        pass

class AgentPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Learning Agents", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.sizer.Add(self.title, (0, 0), flag=wx.ALL)

        self.load_agent_choice = wx.ComboBox(self, -1, choices=AGENTS, name="Agent box")
        self.load_agent_choice.IsEditable = False

        self.load_agent_button = wx.Button(self, -1, "Load New Agent")
        self.Bind(wx.EVT_BUTTON, self.on_load_agent, self.load_agent_button)

        self.update_agent_button = wx.Button(self, -1, "Update Agent")
        self.Bind(wx.EVT_BUTTON, self.on_update_agent, self.update_agent_button)

        self.learn_button = wx.Button(self, -1, "Learn")
        self.Bind(wx.EVT_BUTTON, self.on_learn, self.learn_button)

        self.evaluate_button = wx.Button(self, -1, "Evaluate")
        self.Bind(wx.EVT_BUTTON, self.on_evaluate, self.evaluate_button)

        self.sizer.Add(self.load_agent_choice, (1, 0), flag=wx.ALL)
        self.sizer.Add(self.load_agent_button, (1, 1), flag=wx.ALL)

        self.min_steps_label = wx.StaticText(self, label="Min Steps")
        self.min_steps = wx.TextCtrl(self)

        self.sizer.Add(self.min_steps_label, (2, 0), flag=wx.ALL)
        self.sizer.Add(self.min_steps, (2, 1), flag=wx.ALL)

        self.max_steps_label = wx.StaticText(self, label="Max Steps")
        self.max_steps = wx.TextCtrl(self)

        self.sizer.Add(self.max_steps_label, (2, 2), flag=wx.ALL)
        self.sizer.Add(self.max_steps, (2, 3), flag=wx.ALL)

        self.learning_rate_label = wx.StaticText(self, label="Learning Rate")
        self.learning_rate = wx.TextCtrl(self)

        self.sizer.Add(self.learning_rate_label, (3, 0), flag=wx.ALL)
        self.sizer.Add(self.learning_rate, (3, 1), flag=wx.ALL)

        self.learning_rate_decay_label = wx.StaticText(self, label="Learning Rate Decay")
        self.learning_rate_decay = wx.TextCtrl(self)

        self.sizer.Add(self.learning_rate_decay_label, (3, 2), flag=wx.ALL)
        self.sizer.Add(self.learning_rate_decay, (3, 3), flag=wx.ALL)

        self.ep_length_label = wx.StaticText(self, label="Episode Length")
        self.ep_length = wx.TextCtrl(self)

        self.sizer.Add(self.ep_length_label, (4, 0), flag=wx.ALL)
        self.sizer.Add(self.ep_length, (4, 1), flag=wx.ALL)

        self.num_rollouts_label = wx.StaticText(self, label="Rollouts per iteration")
        self.num_rollouts= wx.TextCtrl(self)

        self.sizer.Add(self.num_rollouts_label, (5, 0), flag=wx.ALL)
        self.sizer.Add(self.num_rollouts, (5, 1), flag=wx.ALL)

        self.convergence_label = wx.StaticText(self, label="Convergence Threshold")
        self.convergence= wx.TextCtrl(self)

        self.sizer.Add(self.convergence_label, (4, 2), flag=wx.ALL)
        self.sizer.Add(self.convergence, (4, 3), flag=wx.ALL)

        self.num_repeats_label = wx.StaticText(self, label="Repeats")
        self.num_repeats= wx.TextCtrl(self)
        self.num_repeats.SetValue("1")

        self.sizer.Add(self.num_repeats_label, (5, 2), flag=wx.ALL)
        self.sizer.Add(self.num_repeats, (5, 3), flag=wx.ALL)

        self.sizer.Add(self.update_agent_button, (6, 0), flag=wx.ALL)
        self.sizer.Add(self.learn_button, (6, 1), flag=wx.ALL)

        self.sizer.Add(self.evaluate_button, (6, 2), flag=wx.ALL)
        
        self.SetSizer(self.sizer)
        self.Layout()

    def on_load_agent(self, event):
        
        choice = self.load_agent_choice.GetValue()

        if agent:
            self.agent = agent.__dict__.get(choice)(self.parent.environment, console=self.parent.console)
            self.display_params()

    def on_evaluate(self, event):

        repeats = int(self.num_repeats.GetValue())
        results = []
        weights = []

        for _ in range(repeats):
            self.agent.environment.random_weights()
            self.on_update_agent(None)
            initial_weights = copy.deepcopy(self.agent.weights)
            #ref_weights = copy.deepcopy(np.concatenate((self.agent.environment.reference.stiffness.weights, self.agent.environment.reference.damping.weights))) 
            result = self.on_learn(None)
            final_weights = copy.deepcopy(self.agent.environment.target.weights)
            results.append(result)
            weights.append((initial_weights, final_weights, results[-1][1]))

            print "Number %d completed" % _

        f = "results-rprop2.yaml"

        #print ref_weights
        print results
        print weights

        data = dict()
        #data['ref_weights'] = ref_weights
        data['results'] = results
        data['weights'] = weights

        with open(f, 'w') as outfile:
            outfile.write(yaml.dump(data))

    def on_learn(self, event):

        return self.agent.evaluate(panels=[self.parent.EnvPlot, self.parent.AgentPlot])

    def on_update_agent(self, event):

        choice = self.load_agent_choice.GetValue()
        self.agent = agent.__dict__.get(choice)(self.parent.environment, console=self.parent.console)
        self.agent.max_steps = int(self.max_steps.GetValue())
        self.agent.min_steps = int(self.min_steps.GetValue())
        self.agent.learning_rate = float(self.learning_rate.GetValue())
        self.agent.learning_rate_decay = float(self.learning_rate_decay.GetValue())
        self.agent.ep_length = float(self.ep_length.GetValue())
        self.agent.num_rollouts = int(self.num_rollouts.GetValue())
        self.agent.convergence_threshold = float(self.convergence.GetValue())

        self.agent.environment.cost.update_time(self.agent.environment.dt, self.agent.ep_length)

    def display_params(self):

        self.max_steps.SetValue(str(self.agent.max_steps))
        self.min_steps.SetValue(str(self.agent.min_steps))
        self.ep_length.SetValue(str(self.agent.ep_length))
        self.num_rollouts.SetValue(str(self.agent.num_rollouts))
        self.convergence.SetValue(str(self.agent.convergence_threshold))

        #Some agents do not require a learning rate
        try:
            self.learning_rate.SetValue(str(self.agent.learning_rate))
            self.learning_rate_decay.SetValue(str(self.agent.learning_rate_decay))
        except:
            pass

class Console(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.out = wx.TextCtrl(self, -1, size=(575, 150), style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL|wx.EXPAND)

    def write(self, string):
        self.out.WriteText(string)

class ParamPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.SIMPLE_BORDER)
        self.parent = parent

        self.sizer = wx.GridBagSizer()

        self.title = wx.StaticText(self, label="Enviroment Parameters", style=wx.ALIGN_CENTRE_HORIZONTAL, size=(200, 20))
        font = wx.Font(16, wx.DECORATIVE, wx.NORMAL, wx.BOLD)
        self.title.SetFont(font)

        self.sizer.Add(self.title, (0, 0), flag=wx.ALL)

def main():

    app = wx.App(False)
    frame = MainWindow(None, "Morphology Learner")
    app.MainLoop()

if __name__ == "__main__":
    main()