#!/usr/bin/env python
import numpy as np
import matplotlib as mpl 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random
import seaborn as sn

'''
This file contains a number of basis functions which can be used to specify a 
stiffness function. Unless otherwise stated, the resulting functions are linear
weighted sums of the basis functions

'''
WEIGHT_MIN = 0.0
WEIGHT_MAX = 20.0
PLOT_MIN = -10
PLOT_MAX = 10
PLOT_POINTS=1000
TANSIG_BASE_WIDTH = 1.0
MAX_IMPULSE = 100000.0

class Basis_1D:
    '''Superclass for all single variable basis functions'''
    '''Mostly used for stiffness functions'''

    def __init__(self, basis_type, mins=[], maxs=[]):

        self.weight_min = WEIGHT_MIN
        self.weight_max = WEIGHT_MAX
        self.plot_min = PLOT_MIN
        self.plot_max = PLOT_MAX
        self.plot_points = PLOT_POINTS
        self.mins = mins
        self.maxs = maxs
        self.type = basis_type

    def plot(self, axes=None, show=False, color='blue'):
        '''Plots the function across the range specified by the PLOT_MAX/PLOT_MIN constants. 
        The axes argument is used to send the plot to a pre-existing axis'''

        #Generate data points
        xs = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        ys = map(self.func, xs)

        #If we are not given a plot axis then make one
        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show=True

        axes.plot(xs, ys, color)
        axes.set_title(self.type)

        if show:
            fig.show()

    def plot_features(self, axes=None, show=False, color='blue'):
        '''Plots the individual basis response across the range specified by the PLOT_MAX/PLOT_MIN constants. 
        The axes argument is used to send the plot to a pre-existing axis'''

        #Generate data points
        xs = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        Ys = map(self.activations, xs)

        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1)
            show=True

        axes.plot(xs, Ys)
        axes.set_title(self.type)

        if show:
            fig.show()

    def perturb(self, ranges):
        '''Perturbs each weight by an amount specified in ranges'''

        if len(ranges) != len(self.weights):
            return 

        new_weights = [self.weights[i] + random.random() * ranges[i] for i in range(len(ranges))]
        self.update(new_weights)

    def step(self, steps):
        '''Changes each weight by the amount specified in steps'''
        if len(steps) != len(self.weights):
            print "Error: Each weight must have a step size, even if it is zero"
            return 

        new_weights = [self.weights[i] + steps[i] for i in range(len(steps))]
        self.update(new_weights)

    def write_metadata(self):
        '''Returns  dictionary containing information about this basis'''
        meta = dict()
        meta['type'] = self.type
        meta['weights'] = self.weights.tolist()
        meta['dimension'] = '1'

        if hasattr(self, 'widths'):
            meta['widths'] = self.widths
        if hasattr(self, 'centres'):
            meta['centres'] = self.centres
        if hasattr(self, 'variances'):
            meta['variances'] = self.variances

        return meta

class Basis_2D:
    '''Superclass for all basis functions'''
    '''Mostly used for damping functions'''

    def __init__(self, basis_type, mins=[], maxs=[]):
        self.weight_min = WEIGHT_MIN
        self.weight_max = WEIGHT_MAX
        self.plot_min = PLOT_MIN
        self.plot_max = PLOT_MAX
        self.plot_points = PLOT_POINTS/10.0
        self.type = basis_type
        self.mins = mins
        self.maxs = maxs

    def plot(self, axes=None, show=False, color='blue'):

        '''Plots the function across the range specified by the PLOT_MAX/PLOT_MIN constants. 
        The axes argument is used to send the plot to a pre-existing axis'''

        #Generate data points
        xs = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        ys = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        Xs, Ys = np.meshgrid(xs, ys)
        zs = np.array([self.func([x, y]) for x, y in zip(np.ravel(Xs), np.ravel(Ys))])
        Zs = zs.reshape(Xs.shape)

        #Make an axis if not given one
        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1, projection='3d')
            show=True

        #Plot the data
        axes.plot_wireframe(Xs, Ys, Zs)
        axes.set_title(self.type)
        axes.set_xlabel('Position')
        axes.set_ylabel('Velocity')
        axes.set_zlabel('Damping force')

        if show:
            fig.show()

    def plot_features(self, axes=None, show=False, color='blue'):

        '''Plots the individual basis response across the range specified by the PLOT_MAX/PLOT_MIN constants. 
        The axes argument is used to send the plot to a pre-existing axis'''

        xs = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        ys = np.linspace(self.plot_min, self.plot_max, self.plot_points)
        Xs, Ys = np.meshgrid(xs, ys)
        zs = np.array([self.activations([x, y]) for x, y in zip(np.ravel(Xs), np.ravel(Ys))])
        Zs = [zs[:, i].reshape(Xs.shape) for i in range(zs.shape[1])]


        if axes is None:
            fig = plt.figure()
            axes = fig.add_subplot(1, 1, 1, projection='3d')
            show=True
        for zs in Zs:
            axes.plot_surface(Xs, Ys, zs)
        axes.set_title(self.type)
        axes.set_xlabel('Position')
        axes.set_ylabel('Velocity')
        axes.set_zlabel('Damping force')

        if show:
            fig.show()


    def perturb(self, ranges):
        '''Perturbs each weight by an amount specified in ranges'''
        if len(ranges) != len(self.weights):
            print "Error: Each weight must have a step size, even if it is zero"
            return 
            
        new_weights = [self.weights[i] + random.random() * steps[i] for i in range(len(steps))]
        self.update(new_weights)

    def step(self, steps):
        '''Changes each weight by an amount specified in ranges'''
        if len(steps) != len(self.weights):
            print "Error: Each weight must have a step size, even if it is zero"
            return 

        new_weights = [self.weights[i] + steps[i] for i in range(len(steps))]
        self.update(new_weights)

    def write_metadata(self):
        '''Returns  dictionary containing information about this basis'''
        meta = dict()
        meta['type'] = self.type
        meta['weights'] = self.weights.tolist()
        meta['dimension'] = '2'

        if hasattr(self, 'widths'):
            meta['widths'] = self.widths
        if hasattr(self, 'centres'):
            meta['centres'] = self.centres
        if hasattr(self, 'variances'):
            meta['variances'] = self.variances

        return meta

class Null(Basis_1D):
    '''Returns 0, always'''
    def __init__(self, num_weights=None):
        Basis_1D.__init__(self, basis_type="Null")
        self.func = lambda x: 0.0
        self.weights = np.array([])
        self.num_weights = 0

    def update(self, weights):
        return

    def random_weights(self):
        return

    def activations(self, state):
        '''Returns a list of feature activations given input x'''
        return []

    def reset(self):
        return

class Null2D(Basis_2D):
    '''Returns 0, always'''
    def __init__(self, num_weights=None):
        Basis_1D.__init__(self, basis_type="Null2D")
        self.func = lambda x: 0.0
        self.weights = np.array([])
        self.num_weights = 0

    def update(self, weights):
        return

    def random_weights(self):
        return

    def activations(self, state):
        '''Returns a list of feature activations given input x'''
        return []

class OddPoly(Basis_1D):

    '''Represents a stiffness function as a weighted sum of polynomials'''
    def __init__(self, weights=[], num_weights=1, zeros=False, mins=[0. ,0.], maxs=[25., 25.]):

        Basis_1D.__init__(self, "OddPoly", mins, maxs)

        if weights:
            self.num_weights = len(weights)
            self.weights = np.array(weights)
            self.update(self.weights)
        elif zeros:
            self.num_weights = num_weights
            self.zero_weights()
        else:
            self.num_weights = num_weights
            self.random_weights()

    def update(self, weights):

        '''Updates the function weights'''
        assert len(weights) == self.num_weights, "Number of weights cannot change %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.weights = np.array(weights)

        self.func = lambda x: np.sum([weight * np.power(x, (2*index)+1) for (index, weight) in enumerate(self.weights)])

    def zero_weights(self):

        self.update([0.0 for _ in range(self.num_weights)])

    def random_weights(self):

        self.update(np.array([self.weight_min + (self.weight_max * random.random()) for _ in range(self.num_weights)]))

    def activations(self, state):
        '''Returns the activation of each polynomial; used to calculate the score function for policy gradient methods'''

        return [np.power(state, (2*index)+1) for (index, weight) in enumerate(self.weights)]


class OddPoly2D(Basis_2D):

    '''Represents a stiffness function as a weighted sum of polynomials'''
    def __init__(self, weights=[], num_weights=1, zeros=False, mins=[0. ,0.], maxs=[25., 25.]):

        Basis_2D.__init__(self, "OddPoly2D", mins, maxs)

        if weights:
            self.num_weights = len(weights)
            self.weights = np.array(weights)
            self.update(self.weights)
        elif zeros:
            self.num_weights = num_weights
            self.zero_weights()
        else:
            self.num_weights = num_weights
            self.random_weights()

    def update(self, weights):

        '''Updates the function weights'''
        assert len(weights) == self.num_weights, "Number of weights cannot change %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.weights = np.array(weights)

        self.func = lambda x: np.sum([weight * np.power(x[1], (2*index)+1) for (index, weight) in enumerate(self.weights)])

    def zero_weights(self):

        self.update([0.0 for _ in range(self.num_weights)])

    def random_weights(self):

        self.update(np.array([self.weight_min + (self.weight_max * random.random()) for _ in range(self.num_weights)]))

    def activations(self, state):
        '''Returns the activation of each polynomial; used to calculate the score function for policy gradient methods'''

        return [np.power(state[1], (2*index)+1) for (index, weight) in enumerate(self.weights)]

class Sig(Basis_1D):
    '''Superclass for sigmoid like basis functions'''

    def __init__(self, weights=[],widths=[], num_weights=1, zeros=False, basis_type="Sigmoid", mins=[], maxs=[]):

        Basis_1D.__init__(self, basis_type, mins, maxs)

        if weights and widths:

            assert len(widths) == len(weights), "Number of weights and widths must match %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

            self.widths = widths
            self.num_weights = len(weights)
            self.weights = np.array(weights)
            self.update(self.weights, self.widths)

        else:
            '''If widths are not specified, create functions with increasing widths'''

            self.num_weights = num_weights
            self.widths = [(TANSIG_BASE_WIDTH*(_+2)) for _ in range(num_weights)]
            if zeros:
                self.zero_weights()
            else:
                self.random_weights()

    def update(self, weights):

        assert len(weights) == self.num_weights, "Number of weights cannot change %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.weights = np.array([max(0, w) for w in weights])

        self.func = lambda x: np.sum([weight * self.base(x/width) for (weight, width) in zip(self.weights, self.widths)])

    def activations(self, state):

        return [self.base(state/width) for width in self.widths]

    def update_widths(self, widths):

        self.update(self.weights, widths)

    def zero_weights(self):

        self.update([0.0 for _ in range(self.num_weights)])

    def random_weights(self):

        self.update(np.array([self.weight_min + (100.0 * self.weight_max * random.random()) for _ in range(self.num_weights)]))


class TanSig(Sig):

    def __init__(self, widths=[], weights=[], num_weights=1, zeros=False):
        '''A sigmoid function used tanh as the base function'''

        self.base = np.tanh
        Sig.__init__(self, widths, weights, num_weights, zeros, "TanSig")

class Sig2D(Basis_2D):
    '''Superclass for sigmoid like basis functions'''

    def __init__(self, weights=[],widths=[], num_weights=1, zeros=False, basis_type="Sigmoid", mins=[], maxs=[]):

        Basis_2D.__init__(self, basis_type, mins, maxs)

        if weights and widths:

            assert len(widths) == len(weights), "Number of weights and widths must match %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

            self.widths = widths
            self.num_weights = len(weights)
            self.weights = np.array(weights)
            self.update(self.weights, self.widths)

        else:
            '''If widths are not specified, create functions with increasing widths'''

            self.num_weights = num_weights
            self.widths = [(TANSIG_BASE_WIDTH*(_+2)) for _ in range(num_weights)]
            if zeros:
                self.zero_weights()
            else:
                self.random_weights()

    def update(self, weights):

        assert len(weights) == self.num_weights, "Number of weights cannot change %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.weights = np.array([max(0, w) for w in weights])

        self.func = lambda x: np.sum([weight * self.base(x[1]/width) for (weight, width) in zip(self.weights, self.widths)])

    def activations(self, state):

        return [self.base(state[1]/width) for width in self.widths]

    def update_widths(self, widths):

        self.update(self.weights, widths)

    def zero_weights(self):

        self.update([0.0 for _ in range(self.num_weights)])

    def random_weights(self):

        self.update(np.array([self.weight_min + (100.0 * self.weight_max * random.random()) for _ in range(self.num_weights)]))


class TanSig2D(Sig2D):

    def __init__(self, widths=[], weights=[], num_weights=1, zeros=False):
        '''A sigmoid function used tanh as the base function'''

        self.base = np.tanh
        Sig2D.__init__(self, widths, weights, num_weights, zeros, "TanSig")

class LinearDamping(Basis_2D):
    def __init__(self, weights=[], zeros=False, num_weights=1, mins=[], maxs=[]):
        '''A basis function for a linear damper - can only accept a single weight'''

        Basis_2D.__init__(self, "LinearDamping", mins, maxs)
        self.num_weights = 1
        if weights:
            self.weights = np.array(weights)
            self.update(weights)
        elif zeros:
            self.zero_weights()
        else:
            self.random_weights()

    def update(self, weights):

        assert len(weights) == 1, "Linear Damping basis has a single weight only"

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]


        self.weights = np.array([max(0, w) for w in weights])
        self.func = lambda x: self.weights[0] * x[1]

    def zero_weights(self):
        self.update([0])

    def random_weights(self):
        self.update([random.random()*(WEIGHT_MAX/1.)])

    def activations(self, state):

        return [state[1]]

class CubicDamping(Basis_2D):
    def __init__(self, weights=[], zeros=False, num_weights=2, mins=[], maxs=[]):
        '''A basis function for a linear damper - can only accept a single weight'''

        Basis_2D.__init__(self, "LinearDamping", mins, maxs)
        self.num_weights = 2
        if weights:
            self.weights = np.array(weights)
            self.update(weights)
        elif zeros:
            self.zero_weights()
        else:
            self.random_weights()

    def update(self, weights):

        assert len(weights) == 2, "Cubic Damping basis has a single weight only"

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]


        self.weights = np.array([max(0, w) for w in weights])
        self.func = lambda x: self.weights[0] * x[1] + self.weights[1] * x[1] **3

    def zero_weights(self):
        self.update([0])

    def random_weights(self):
        self.update([10 * WEIGHT_MIN + random.random()*WEIGHT_MAX, 10 * WEIGHT_MIN + random.random()*WEIGHT_MAX])

    def activations(self, state):

        return [state[1], state[1]**3]


class Stiffness(Basis_1D):

    def __init__(self, weights=[], num_tan=2, num_poly=2, num_weights=4, zeros=False, mins=[0., 0., 0., 0.], maxs=[]):
        '''A combined basis including a polynomial and tansig component'''

        Basis_1D.__init__(self, "Stiffness", mins, maxs)

        if num_tan and num_poly:
            self.num_weights = num_tan + num_poly
            self.num_tan = num_tan
            self.num_poly = num_poly

        if weights:
            self.weights = np.array(weights)
            self.update(weights)
        elif zeros:
            self.zero_weights()
        else:
            self.random_weights()

    def update(self, weights):

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.tan_func = TanSig(num_weights=self.num_tan, widths=[0.2, 5.])
        self.tan_func.update(weights[0:self.num_tan])

        self.poly_func = OddPoly(num_weights=self.num_poly)
        self.poly_func.update(weights[self.num_tan:])

        self.weights = np.array(weights)

        self.func = lambda x: self.tan_func.func(x) + self.poly_func.func(x)

        print self.weights

    def zero_weights(self):
        self.update([0 for w in self.weights])

    def random_weights(self):
        new_weights = [WEIGHT_MIN + random.random() * WEIGHT_MAX for w in range(self.num_tan)] + [WEIGHT_MIN + random.random() * WEIGHT_MAX for w in range(self.num_poly)]
        self.update(new_weights)

    def activations(self, state):

        activations =  self.tan_func.activations(state) + self.poly_func.activations(state)

        return activations

class Damping(Basis_2D):

    def __init__(self, weights=[], num_tan=2, num_poly=2, num_weights=4, zeros=False, mins=[0., 0., 0., 0.], maxs=[]):
        '''A combined basis including a polynomial and tansig component'''

        Basis_2D.__init__(self, "Damping", mins, maxs)

        if num_tan and num_poly:
            self.num_weights = num_tan + num_poly
            self.num_tan = num_tan
            self.num_poly = num_poly

        if weights:
            self.weights = np.array(weights)
            self.update(weights)
        elif zeros:
            self.zero_weights()
        else:
            self.random_weights()

    def update(self, weights):

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.tan_func = TanSig2D(num_weights=self.num_tan, widths=[0.2, 5.])
        self.tan_func.update(weights[0:self.num_tan])

        self.poly_func = OddPoly2D(num_weights=self.num_poly)
        self.poly_func.update(weights[self.num_tan:])

        self.weights = np.array(weights)

        self.func = lambda x: self.tan_func.func(x) + self.poly_func.func(x)

        print self.weights

    def zero_weights(self):
        self.update([0 for w in self.weights])

    def random_weights(self):
        new_weights = [WEIGHT_MIN + random.random() * WEIGHT_MAX for w in range(self.num_tan)] + [WEIGHT_MIN + random.random() * WEIGHT_MAX/4. for w in range(self.num_poly)]
        self.update(new_weights)

    def activations(self, state):

        activations =  self.tan_func.activations(state) + self.poly_func.activations(state)

        return activations

class Leonard(Basis_2D):
    '''Basis function for a leonard system; gauranteed to have a unique stable limit cycle'''
    '''If the strict parameter is set to false the constant term may become positive, leading to a damped system'''
    def __init__(self, weights=[], num_weights=2, zeros=False, strict=True, mins=[], maxs=[]):

        Basis_2D.__init__(self, "Leonard", mins, maxs)
        self.strict = strict
        if weights:
            self.num_weights = len(weights)
            self.weights = np.array(weights)
            self.update(self.weights)
        elif zeros:
            self.num_weights = num_weights
            self.zero_weights()
        else:
            self.num_weights = num_weights
            self.random_weights()

    def update(self, weights):

        assert len(weights) == self.num_weights, "Number of weights cannot change %s: (%d, %d)" % (self.type, len(weights), len(self.num_weights))

        if self.mins and len(self.mins) == len(weights):
            weights = [max(w, m) for (w, m) in zip(weights, self.mins)]

        if self.maxs and len(self.maxs) == len(weights):
            weights = [min(w, m) for (w, m) in zip(weights, self.maxs)]

        self.weights = np.array(weights)
        self.func = lambda x: np.sum([x[1] * weight * np.power(x[0], (2*index)) for (index, weight) in enumerate(weights)])

    def zero_weights(self):

        self.update([0.0 for _ in range(self.num_weights)])

    def random_weights(self):

        weights = np.array([self.weight_min + (self.weight_max * random.random()) for _ in range(self.num_weights)])

        if self.strict or random.random() < 0.5:
            weights[0] *= -1.0

        self.update(np.array(weights))

    def activations(self, state):
        return [state[1]  * np.power(state[0], (2*index)) for (index, weight) in enumerate(self.weights)]

class Pulse:
    '''Class for representing impulse forces which can be applied at the beginning of an episode.
    Unlike the impulse class, these forces are applied over a number of timesteps.
    This behaviour is implemented with a simple gaussian in time
    '''

    def __init__(self, weight=None, zeros=False, num_weights=1, trigger_time=0.0, time_decay = 0.1):

        self.trigger_time = trigger_time
        self.time_decay = time_decay

        if zeros:
            self.zero_weights()
        elif weight:
            self.update(weight)
        else:
            if num_weights == 1:
                self.random_weights()
            else:
                print "Impulse basis can only have a single weight"

        
        self.num_weights = 1
        self.type = "Impulse"

    def perturb(step):
        weight += np.random() * step
        self.update(weight)

    def update(self, weight):
        self.weights = weight

    def func(self, t):

        signal = self.weights[0] * np.exp(-((t-self.trigger_time)**2)/self.time_decay)

        return signal

    def activations(self, time):
        return np.exp(-((t-self.trigger_time)**2)/self.time_decay)

    def zero_weights(self):
        self.update([0])

    def random_weights(self):
        weight = -MAX_IMPULSE + (np.random.rand() * 2 * MAX_IMPULSE)
        self.update([weight])


'''
Super class to represent external controllers
'''

class Controller:

    def __init__(self, umax, umin, clip, num_weights):

        self.num_weights = num_weights
        if self.num_weights == 0:
            self.fixed = True
        else:
            self.fixed = False

        self.umax = umax
        self.umin = umin
        self.clip = clip

'''
A controller which applies a constant force
'''

class ConstForce(Controller):

    def __init__(self, num_weights=0, force=20.0, t_start=0, t_end=10.):
        Controller.__init__(self, force*2, 0, False, num_weights)
        self.t_start = t_start
        self.t_end = t_end
        self.weights = [force]

    def func(self, state):

        (system, t) = state

        if t > self.t_start and t < self.t_end:
            return float(self.weights[0])
        else:
            return 0.0

    def update(self, force):

        self.weights = force

    def activations(self, state):

        (system, t) = state

        if t > self.t_start and t < self.t_end:
            return [1.0]
        else:
            return [0.0]

    def reset(self):
        pass

'''
A PD controller with fixed weights
'''

class PD(Controller):

    def __init__(self, reference, num_weights, gains=[1000.0, 50.0], umax=1000.0, umin=1000.0, clip=False):
        '''
        Num_weights determines the number of adaptable weights, not the total number of weights
        For example, if num_weights is 1, then the proportional gain can be adjusted by the agent, but the derivative term is fixed
        '''
        
        Controller.__init__(self, umax, umin, clip, num_weights)

        self.last_error = 0.0
        self.kp = gains[0]
        self.kd = gains[1]
        self.weights = gains[0:num_weights]
        self.reference = reference
        self.refresh_rate = 0.001
        self.type = "PD"

    def func(self, state):

        #Split input and state and timestep
        (system, t) = state

        #Calculate error and error derivative
        setpoint = self.reference.outputs[int(t/self.reference.dt)]
        error = setpoint - system[0]
        error_derivative = self.last_error - error

        #Calculae control signal
        signal = (-((self.kp*error) + (self.kd*error_derivative))) * (((t/self.reference.dt)%(self.refresh_rate/self.reference.dt))==0)

        #Store history
        self.last_error = error
        self.last_activation = [-error, -error_derivative]

        #Apply control limits
        if self.clip:
            signal = min(max(self.umin, signal), self.umax)

        return signal

    def update(self, gains):

        if len(gains) == 0:
            return

        if len(gains) == 1:
            self.kp = max(gains[0], 0.0)
            self.weights = [self.kp]

        if len(gains) == 2:
            self.kp = max(gains[0], 0.0)
            self.kd = max(gains[1], 0.0)
            self.weights = [self.kp, self.kd]

    def activations(self, state):

        return self.last_activation

    def reset(self):

        self.last_error = 0.0

class Gaussian(Controller):

    def __init__(self, num_weights, weights=[], times=[], deviations=[], umax=1000.0, umin=1000.0, clip=False):
        
        Controller.__init__(self, umax, umin, clip, num_weights)
        
        '''
        Use default values for now
        '''

        weights=[1. for _ in range(10)]
        times=[0., 1., 2., 3., 4., 5., 6., 7., 8., 9.,]
        deviations = [0.1 for _ in range(10)]

        self.weights = weights
        self.num_weights = len(weights)
        self.times = times
        self.deviations = deviations
        self.type = "Gaussian"

    def func(self, state):

        #Split input and state and timestep
        (system, t) = state

        activations = [np.exp((-1*((t-time)**2))/dev) for (time, dev) in zip(self.times, self.deviations)]
        signal = np.sum([a*w for (a, w) in zip(activations, self.weights)])

        return signal

    def update(self, weights):

        self.weights = weights

    def activations(self, state):

        (system, t) = state

        activations = [np.exp((-1*((t-time)**2))/dev) for (time, dev) in zip(self.times, self.deviations)]

        return activations

    def reset(self):

        pass

class DMP(Controller):

    def __init__(self, num_weights, weights=[], times=[], deviations=[], umax=1000.0, umin=1000.0, clip=False):
        Controller.__init__(self, umax, umin, clip, num_weights)

        if not weights:
            weights = [0.0 for _ in range(num_weights)]

        times = [0.01, 1., 2., 3., 4., 5., 6., 7., 8., 9.]
        deviations = [0.1 for c in times]

        '''
        self.ws are the weights used to calculate the signal
        self.weights is the list of those ws which are exposed to learning
        '''
        self.ws = weights
        self.weights = self.ws[0:num_weights]
        self.num_weights = num_weights
        self.times = times
        self.deviations = deviations
        self.type = "DMP"

    def func(self, state):

        (system, t) = state

        activations = [np.exp((-1*((t-time)**2))/dev) for (time, dev) in zip(self.times, self.deviations)]
        signal = (np.sum([a*w for (a, w) in zip(activations, self.ws)]) / np.sum(activations)) * np.exp(-t/100.)

        return signal

    def update(self, weights):

        if weights:
            self.ws = weights
            self.weights = self.ws[0:self.num_weights]

    def activations(self, state):

        (system, t) = state

        activations = [(np.exp((-1*((t-time)**2))/dev)*np.exp(-t/100.)) for (time, dev) in zip(self.times, self.deviations)]
        activations_sum = np.sum(activations)
        activations = [a/activations_sum for a in activations]

        return activations

    def reset(self):

        pass






