import matplotlib.pyplot as plt
import matplotlib
import seaborn as sn
import numpy as np
import yaml

f = "results-poly15.yaml"

results = yaml.load(open(f, "r"))


costs = results['results']
start_costs = [v[0] for v in costs]
final_costs = [v[1] for v in costs]
relative_costs = [v[1]/v[0] for v in costs]

weights = results['weights']
init_dists = list()
final_dists = list()
low_costs = list()
all_costs = list()
init_w1s = list()
init_w2s = list()
init_w1_dists = list()
init_w2_dists = list()
w1s = list()
w2s = list()
high_costs = list()

ref = results['ref_weights']
init_ref1 = ref[0]
init_ref2 = ref[1]

for index, line in enumerate(weights):
	init_w1 =  line[0][0]
	init_w2 =  line[0][1]
	final_w1 =  line[1][0]
	final_w2 =  line[1][1]

	init_w1s.append(init_w1)
	init_w2s.append(init_w2)

	cost = line[2]

	init_dist = np.sqrt((init_w1 - init_ref1)**2 + (init_w2 - init_ref2)**2)
	final_dist = np.sqrt((final_w1 - init_ref1)**2 + (final_w2 - init_ref2)**2)


	init_dists.append(init_dist)
	final_dists.append(final_dist)
	w1s.append(final_w1)
	w2s.append(final_w2)
	low_costs.append(cost)
	high_costs.append(costs[index][0])
	all_costs.append(costs[index][1])

	w1s.append(init_w1)
	w2s.append(init_w2)
	all_costs.append(costs[index][0])

print all_costs


def make_success_fig():
	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	ax.scatter(init_dists, low_costs)
	ax.set_yscale('log')
	ax.set_xlabel('Start dist', fontsize=12)
	ax.set_ylabel('Costs', fontsize=12)
	ax.tick_params(axis='both', which='major', labelsize=12)

def make_cost_fig():

	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	plt.scatter(w1s, w2s, c=all_costs, norm=matplotlib.colors.LogNorm(), cmap='RdBu_r')
	ax.set_ylabel('Cubic Weights', fontsize=12)
	ax.set_xlabel('Linear Weights', fontsize=12)
	plt.colorbar()
	ax.tick_params(axis='both', which='major', labelsize=12)

def make_success_fig2():

	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	plt.scatter(init_w1s, init_w2s, c=low_costs, norm=matplotlib.colors.LogNorm(), cmap='RdBu_r')
	ax.set_ylabel('Cubic Weights', fontsize=12)
	ax.set_xlabel('Linear Weights', fontsize=12)
	plt.colorbar()
	ax.tick_params(axis='both', which='major', labelsize=12)

def make_cost_fig2():

	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	plt.scatter(high_costs, low_costs)





make_success_fig()
plt.tight_layout()
plt.show()
make_cost_fig()
plt.tight_layout()
plt.show()
make_success_fig2()
plt.tight_layout()
plt.show()
make_cost_fig2()
plt.tight_layout()
plt.show()
